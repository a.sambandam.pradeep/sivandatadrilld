@php
    $configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="dashboard-analytics">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">DrillD</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <!--<li>
                <h3 class="text-bold-600 color-dblack mt-2">Filters</h3>
				<form class="form form-vertical" id="filterform" action="{{ url('results-dashboard')}}" method="POST">
                          <div class="form-body">
                              <div class="row">
                              <div class="col-12">
                                      <button type="reset" class="btn btn-primary mr-1 mb-1 col-12 btn-spurple font-medium-5 resetbtn">Reset search</button>
                                  </div>
                                  <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">location_on</span> Region</span>
                                            </div>                                            

											<select name="local" id="local" data-value="{{ $local }}" class="form-control form-control font-medium-3 dynamic local" data-dependent="dp">
                                                <option value="">Choose a city</option>
												 @foreach($regions as $region)
													@if( $region->local == $local)
														<option value="{{ $region->local }}" selected>{{ $region->local }}</option>
													@else
														<option value="{{ $region->local }}">{{ $region->local }}</option>
													@endif	
												 @endforeach
											</select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">date_range</span> Date range</span>
                                            </div>
                                            
                                            <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="fromdate" id="fromdate" placeholder="From" value="{{ $fromdate }}" />
                                            <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="todate" id="todate" placeholder="To" value="{{ $todate }}" />
                                            </div>
                                        </fieldset>
                                  </div>
                                  <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">store</span> Department</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="dp" id="dp" data-value="{{ $dp }}" data-dependent="category" data-label="Choose a department">
                                                <option value="">Choose a department</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Category</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="category" id="category" data-value="{{ $category }}" data-dependent="brand" data-label="Choose a category">
                                                <option value="">Choose a category</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Brand</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="brand" id="brand" data-value="{{ $brand }}" data-dependent="product" data-label="Choose a brand (optional)">
                                                <option value="">Choose a brand (optional)</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Product</span>
                                            </div>

                                            <select class="form-control font-medium-3" name="product" id="product" data-value="{{ $product }}" data-label="Choose a product (optional)">
                                                <option value="">Choose a product (optional)</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                    <div class="col-12">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="initload" value="no" />
                                      <button type="submit" class="btn btn-primary mr-1 mb-1 col-12 btn-spurple font-medium-5">חיפוש מידע</button>
                                  </div>
                              </div>
                          </div>
                      </form>
            </li>-->
            <li>
                <a href="/search-information" class="d-flex tabref" data-tabid="1">
                    <img src="{{asset('images/icons/1.png') }}" alt="Comparisons" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Comparisons</strong></span>
                </a>
            </li>
            <li class="active">
            <a href="/search-information" class="d-flex tabref" data-tabid="2">
                    <img src="{{asset('images/icons/2.png') }}" alt="Buyers" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Buyers</strong><br /><span>Demographic+ buyer personality</span></span>
                </a>
            </li>
            <li>
            <a href="/search-information" class="d-flex tabref" data-tabid="3">
                    <img src="{{asset('images/icons/3.png') }}" alt="Conversion funnel" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Conversion funnel</strong><br /><span>rush hours/conversion rate/undecided/abandonment</span></span>
                </a>
            </li>
            <li>
            <a href="/search-information" class="d-flex tabref" data-tabid="4">
                    <img src="{{asset('images/icons/4.png') }}" alt="Pricing and promotion analysis" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Pricing and promotions <br />analysis</strong></span>
                </a>
            </li>
            <li>
            <a href="/search-information" class="d-flex tabref" data-tabid="5">
                    <img src="{{asset('images/icons/5.png') }}" alt="Planogram analysis" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Planogram analysis</strong></span>
                </a>
            </li>
            <li>
            <a href="/search-information" class="d-flex tabref" data-tabid="6">
                    <img src="{{asset('images/icons/6.png') }}" alt="Decision tree" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Decision tree</strong></span>
                </a>
            </li>
            <li>
            <a href="/search-information" class="d-flex tabref" data-tabid="7">
                    <img src="{{asset('images/icons/7.png') }}" alt="Shelf behavior" height="64" width="64" /> &nbsp;&nbsp;&nbsp;
                    <span class="title ml-10"><strong>Shelf behavior</strong><br /><span>Nano behavior data+Avatar</span></span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
