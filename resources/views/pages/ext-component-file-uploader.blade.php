@extends('layouts/contentLayoutMaster')

@section('title', 'Upload Data')

@section('vendor-style')
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/ui/prism.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
@endsection
@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection
@section('content')
<!-- Dropzone section start -->
<section id="dropzone-examples">

  <!-- accepted file upload starts -->
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">העלה נתוני CSV</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form action="{{ url('upload-csv')}}" class="dropzone dropzone-area" id="dp-accept-files" method="POST">
              <div class="dz-message">שחרר קבצים לכאן להעלאה</div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- accepted file upload ends -->
</section>
<style>
html body {
    overflow: hidden;
}
</style>
<!-- // Dropzone section end -->
@endsection

@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/ui/prism.min.js')) }}"></script>
@endsection
@section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('js/scripts/extensions/dropzone.js')) }}"></script>
@endsection
