
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
		<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
		@if($noresults != 0)
		<link rel="stylesheet" href="{{ asset(mix('css/pages/error.css')) }}">
		@endif
        <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-ecommerce.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
@endsection
@section('content')
  {{-- Dashboard Ecommerce Starts --}}
  @if($noresults == 0)
    <section id="dashboard-ecommerce">

        <!-- Tab 1 Starts -->
        <div id="tab1data" class="tabcontent">
            <div class="row match-height firstrow j-center mh-80">
                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Time line comparison</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="col-12 form-group">
                                    <fieldset>
                                        <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">date_range</span> Date range</span>
                                        </div>

                                        <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="fromdate" id="fromdate" placeholder="From" />
                                        <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="todate" id="todate" placeholder="To" />
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                        <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">date_range</span> Date range</span>
                                        </div>

                                        <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="fromdate" id="fromdate" placeholder="From" />
                                        <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="todate" id="todate" placeholder="To" />
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <button type="submit" class="btn btn-outline-primary mb-1 col-12 font-medium-5 waves-effect waves-light"><i class="fa fa-plus-circle pad-rl0"></i> Time line comparison</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Competition comparison</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                            <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Brand</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="brand" id="brand" data-dependent="product" data-label="Choose a brand (optional)">
                                                <option value="">Choose a brand (optional)</option>
                                                @foreach($brands as $branddata)
												 <option value="{{ $branddata->brand }}">{{ $branddata->brand }}</option>
												 @endforeach
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Product</span>
                                            </div>

                                            <select class="form-control font-medium-3" name="product" id="product" data-label="Choose a product (optional)">
                                                <option value="">Choose a product (optional)</option>
                                                @foreach($products as $productdata)
												 <option value="{{ $productdata->product }}">{{ $productdata->product }}</option>
												 @endforeach
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                
                                <div class="col-12 form-group">
                                    <button type="submit" class="btn btn-outline-primary mb-1 col-12 font-medium-5 waves-effect waves-light"><i class="fa fa-plus-circle pad-rl0"></i> Competition comparison</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Region comparison</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">location_on</span> City</span>
                                            </div>

											<select name="local" id="local" class="form-control form-control font-medium-3 dynamic" data-dependent="dp">
												 <option value="">Choose a city</option>
												 @foreach($regions as $region)
												 <option value="{{ $region->local }}">{{ $region->local }}</option>
												 @endforeach
											</select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">location_on</span> Region</span>
                                            </div>

											<select name="local" id="local" class="form-control form-control font-medium-3 dynamic" data-dependent="dp">
												 <option value="">Choose a Region</option>
												 @foreach($regions as $region)
												 <option value="{{ $region->local }}">{{ $region->local }}</option>
												 @endforeach
											</select>
                                            </div>
                                        </fieldset>
                                </div>
                                
                                <div class="col-12 form-group">
                                    <button type="submit" class="btn btn-outline-primary mb-1 col-12 font-medium-5 waves-effect waves-light"><i class="fa fa-plus-circle pad-rl0"></i> Region comparison</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Tab 1 Ends -->

        <!-- Tab 2 Starts -->
        <div id="tab2data" class="tabcontent active">
            <div class="row d-flex justify-content-between align-items-end pad-bottom20">
                <div class="col-md-1 col-6">
                    <h3 class="text-bold-200 color-dblack">Buyer Profile</h3>
                </div>
                <div class="col-md-11 col-6">
                    <hr />
                </div>
            </div>
            <div class="row match-height firstrow">
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Average Age</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div id="chart1" class="col-md-6 col-12 doughcharts mb-1" data-labels="18 - 25,25 - 35,35 - 45,45 - 55,55 - 65,65+" data-percentages="{{ $agePercentages }}" data-colors="#5A30DC,#9013FE,#DAB0E8,#b561cf,#DA4CB2,#EB87CF"data-totalval="{{ $productavgage }}" data-labelcolors="#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A,#4A4A4A"></div>
                                <div class="col-md-3 col-6">
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color1"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">18 - 25</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color2"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">25 - 35</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color3"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">35 - 45</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-6">
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color4"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">45 - 55</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color5"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">55 - 65</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color6"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">65+</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="m-10" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="series-result">
                                    @if($productavgage > $categoryavgage)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>{{ $differenceage }} years older than the category average</span>
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>{{ $differenceage }} years younger than the category average</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Gender Segmentation</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[1] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div id="chart2" class="col-md-6 col-12 doughcharts mb-1" data-labels="Women, Men" data-icon="f007" data-percentages="{{ $genderPercentages }}" data-colors="#19ABDE,#186BA1" data-labelcolors="#4A4A4A,#4A4A4A"></div>
                                <div class="col-md-3 col-6">
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color7"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Women</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color8"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Men</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-6">

                                </div>
                            </div>
                        </div>
                        <hr class="m-10" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="series-result">
                                    @if($productMalePercent > $categoryMalePercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏{{ $productMalePercent-$categoryMalePercent }}% more men than average in category</span><br />
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>{{ $categoryMalePercent-$productMalePercent }}% fewer men than the average in the category</span><br />
                                    @endif

                                    @if($productFemalePercent > $categoryFemalePercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏&rlm;{{ $productFemalePercent-$categoryFemalePercent }}% more women than average in the category</span>
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>&rlm;{{ $categoryFemalePercent-$productFemalePercent }}% fewer women than average in the category</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Buyers Personality -->
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Type of purchase</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true"  title="{{ $tootltips[2] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div id="chart3" class="col-md-6 col-12 doughcharts mb-1" data-labels="Emotional, Rational" data-icon="f07a" data-percentages="{{ $emotional_buyers }},{{$rational_buyers}}" data-colors="#DA4CB2,#AF4BCF" data-labelcolors="#4A4A4A,#4A4A4A"></div>
                                <div class="col-md-3 col-6">
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color9"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Emotional</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color10"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Rational</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-6">

                                </div>
                            </div>
                        </div>
                        <hr class="m-10" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="series-result">
                                    @if($productRPercent > $categoryRPercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏{{ $productRPercent-$categoryRPercent }}% more Rational than average in the category</span><br />
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>{{ $categoryRPercent-$productRPercent }}% fewer Rational than average in the category</span><br />
                                    @endif

                                    @if($productEPercent > $categoryEPercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏&rlm;{{ $productEPercent-$categoryEPercent }}% more Emotional than average in the category</span>
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>&rlm;{{ $categoryEPercent-$productEPercent }}% fewer Emotional than average in the category</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Family Composition</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[3] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div id="chart4" class="col-md-6 col-12 doughcharts mb-1" data-labels="Single, Families, Couples" data-icon="f0c0" data-percentages="{{ $compositionPercentages }}" data-colors="#EABE3B,#EF7E32,#DE542D" data-labelcolors="#4A4A4A,#4A4A4A,#4A4A4A"></div>
                                <div class="col-6">
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color11"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Single</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color12"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Families</span>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            <i class="fa fa-circle font-medium-2 text-color13"></i>
                                            <span class="text-bold-600 mx-50 color-lblack">Couples</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="m-10" />
                        <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="series-result">
                                    @if($productSinglePercent > $categorySinglePercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏{{ $productSinglePercent-$categorySinglePercent }}% more Single person than the average in the category</span><br />
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>{{ $categorySinglePercent-$productSinglePercent }}% fewer Single person than the average in the category</span><br />
                                    @endif

                                    @if($productFamilyPercent > $categoryFamilyPercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏&rlm;{{ $productFamilyPercent-$categoryFamilyPercent }}% more Families than average in the category</span><br />
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>&rlm;{{ $categoryFamilyPercent-$productFamilyPercent }}% fewer Families than average in the category</span> <br />
                                    @endif

                                    @if($productCouplePercent > $categoryCouplePercent)
                                        <i class="feather icon-arrow-up color-green"></i>
                                        <span>‏&rlm;{{ $productCouplePercent-$categoryCouplePercent }}% more Couples than average in the category</span>
                                    @else
                                        <i class="feather icon-arrow-down color-orange"></i>
                                        <span>&rlm;{{ $categoryCouplePercent-$productCouplePercent }}% more Couples than average in the category</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row secondrow">
                
                <!-- Impulse Buying -->
                <div class="col-lg-6 col-12 match-height">
                    <div class="card mb-1 col-12 carddata" >
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="brain" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-brainx" style="width: 15px;"><path fill="currentColor" d="M208 0c-29.9 0-54.7 20.5-61.8 48.2-.8 0-1.4-.2-2.2-.2-35.3 0-64 28.7-64 64 0 4.8.6 9.5 1.7 14C52.5 138 32 166.6 32 200c0 12.6 3.2 24.3 8.3 34.9C16.3 248.7 0 274.3 0 304c0 33.3 20.4 61.9 49.4 73.9-.9 4.6-1.4 9.3-1.4 14.1 0 39.8 32.2 72 72 72 4.1 0 8.1-.5 12-1.2 9.6 28.5 36.2 49.2 68 49.2 39.8 0 72-32.2 72-72V64c0-35.3-28.7-64-64-64zm368 304c0-29.7-16.3-55.3-40.3-69.1 5.2-10.6 8.3-22.3 8.3-34.9 0-33.4-20.5-62-49.7-74 1-4.5 1.7-9.2 1.7-14 0-35.3-28.7-64-64-64-.8 0-1.5.2-2.2.2C422.7 20.5 397.9 0 368 0c-35.3 0-64 28.6-64 64v376c0 39.8 32.2 72 72 72 31.8 0 58.4-20.7 68-49.2 3.9.7 7.9 1.2 12 1.2 39.8 0 72-32.2 72-72 0-4.8-.5-9.5-1.4-14.1 29-12 49.4-40.6 49.4-73.9z" class=""></path></svg> Impulse buying index  </h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[13] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                        <div class="card-body pt-0">
                            <p class="color-dblack">Inclining the data between the seconds of finding the product and the seconds of making the decision and the percentage of abandonment</p>
                            <!-- <p class="color-dblack">מדד משוכלל למספר נתונים כגון: זמן מציאת המוצר, זמן התלבטות הרכישה, זגזוג בין מוצרים / מותגים שונים ועוד. <br />המטרה היא להגדיל מדד זה על מנת ליצור רכישה אימפולסיבית ככל הניתן ובעלת השפעה על הקונה בזמן הקניה. <br />ככל שהרכישה יותר אימפולסיבית לצרכן יש דחף בלתי רציונלי לרכוש מוצר כלשהו בנקודת הרכישה. <br />ככל שמדד האימפולס עולה כך יורד מדד הרכישה הרציונלית.</p> -->
                            <div class="barwrap" style="min-height:360px;">
                                <!--<canvas id="bar-chart" data-values="{{ $productImpulse }},{{ $brandImpulse }},{{ $categoryImpulse }}"></canvas>-->
                                <canvas id="bar-chart" data-values="82,60,22"></canvas>
                            </div>
                            <div class="col-12">
                                @if($productImpulse > $categoryImpulse)
                                    <i class="feather icon-arrow-up color-green"></i>
                                    <span class="font-medium-1 text-bold-600">From the average in the category {{$productImpulse-$categoryImpulse}}% more impulses in the product</span><br />
                                @else
                                    <i class="feather icon-arrow-down color-orange"></i>
                                    <span class="font-medium-1 text-bold-600">From the average in the category {{$categoryImpulse-$productImpulse}}% fewer impulses in the product</span><br />
                                @endif
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-6 col-12 match-height">
                    <div class="card mb-1 col-12 carddata">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">Demographic segmentation</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[15] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>                    
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0">
                                <div class="barwrap">
                                    <canvas id="groupChart"></canvas>
                                </div>					
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Tab 2 Ends -->        

        <!-- Tab 3 Starts -->
        <div id="tab3data" class="tabcontent">
            <div class="row secondrow mh-80 j-center">
                <div class="col-lg-8 col-12">
                    <div class="card mb-1 col-12">
                        <div class="card-header d-flex justify-content-between align-items-end">
                            <h4 class="text-bold-800 color-dblack">In-store audience involvement</h4>
                            <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[4] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                        </div>
                        <hr class="m-10 mb-2" />
                        <div class="card-content">
                            <div class="card-body pt-0">
                            <style>
                            #chartdiv {
                            width: 100%;
                            height: 250px;
                            }

                            </style>

                            <!-- Resources -->
                            <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
                            <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
                            <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

                            <!-- Chart code -->
                            <script>
                            am4core.ready(function() {

                            // Themes begin
                            am4core.useTheme(am4themes_animated);
                            // Themes end

                            var chart = am4core.create("chartdiv", am4charts.SlicedChart);
                            chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

                            chart.data = [{
                                "name": "Class interactions",
                                "value": 600
                            }, {
                                "name": "Category Interactions",
                                "value": 300
                            }, {
                                "name": "Brand Interactions",
                                "value": 200
                            }, {
                                "name": "Product Interactions",
                                "value": 180
                            }, {
                                "name": "Product purchase",
                                "value": 50
                            }, {
                                "name": "Abandonment during purchase",
                                "value": 20
                            }];

                            var series = chart.series.push(new am4charts.FunnelSeries());
                            series.colors.step = 2;
                            series.dataFields.value = "value";
                            series.dataFields.category = "name";
                            series.alignLabels = true;

                            series.labelsContainer.paddingLeft = 15;
                            series.labelsContainer.width = 250;

                            //series.orientation = "horizontal";
                            //series.bottomRatio = 1;

                            chart.legend = new am4charts.Legend();
                            chart.legend.position = "left";
                            chart.legend.valign = "bottom";
                            chart.legend.margin(5,5,20,5);

                            }); // end am4core.ready()
                            </script>

                            <!-- HTML -->
                            <div id="chartdiv"></div>

                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-end twcolrow">
                        <div class="card col-12 col-md-6 modified">
                            <div class="card-header d-flex justify-content-between align-items-center pad-rl0">
                                <h4 class="text-bold-800 color-dblack"><span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[11] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span> Product abandonment</h4>
                                <p class="font-medium-1 mb-0 text-bold-400 color-dblack">{{ $product }}</p>
                            </div>
                            <hr class="m-10 mb-2" />
                            <div class="card-content">
                                <div class="card-body pt-0">
                                    <div class="features row">
                                        <div class="col-12">
                                            <div class="progress bg-orange-pbar progress-xl">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $productAbandonPercentage }}" aria-valuemin="{{ $productAbandonPercentage }}" aria-valuemax="100" style="width:{{ $productAbandonPercentage }}%">{{ $productAbandonPercentage }}%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            @if($productAbandonPercentage > $totalDpCartPercentage)
                                                <i class="feather icon-arrow-up color-green"></i>
                                                <span class="font-medium-1 text-bold-600 color-green">‏{{ $productAbandonPercentage-$totalDpCartPercentage }}% Above average</span><br />
                                            @else
                                                <i class="feather icon-arrow-down color-orange"></i>
                                                <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$productAbandonPercentage }}% Below average</span><br />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card col-12 col-md-6 modified">
                            <div class="card-header d-flex justify-content-between align-items-center pad-rl0">
                                <h4 class="text-bold-800 color-dblack"><span class="font-medium-1 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[12] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></span>  Category conversion ratio</h4>
                                <p class="font-medium-1 mb-0 text-bold-400 color-dblack">{{ $category }}</p>
                            </div>
                            <hr class="m-10 mb-2" />
                            <div class="card-content">
                                <div class="card-body pt-0">
                                    <div class="features row">
                                        <div class="col-12">
                                            <div class="progress bg-blue-pbar progress-xl">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $categoryInteractionPercentage }}" aria-valuemin="{{ $categoryInteractionPercentage }}" aria-valuemax="100" style="width:{{ $categoryInteractionPercentage }}%">{{ $categoryInteractionPercentage }}%</div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            @if($categoryInteractionPercentage > $totalDpCartPercentage)
                                                <i class="feather icon-arrow-up color-green"></i>
                                                <span class="font-medium-1 text-bold-600 color-green">‏{{ $categoryInteractionPercentage-$totalDpCartPercentage }}% Above Average</span><br />
                                            @else
                                                <i class="feather icon-arrow-down color-orange"></i>
                                                <span class="font-medium-1 text-bold-600 color-orange">{{ $totalDpCartPercentage-$categoryInteractionPercentage }}% Below Average</span><br />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <!-- Flow Chart -->
                <div class="card mb-1 col-lg-4 flowchartpart" style="height:543px">
                            <div class="card-header d-flex justify-content-between align-items-end">
                                <h4 class="text-bold-800 color-dblack"> Products that encourage impulse buying </h4>
                                <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="המטרה היא להגדיל את מדד האימופלס ולמדוד את המותג שלנו לעומת המדד בקטגוריה"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                            </div>
                    <div class="card-content">
                            <div class="card-body pt-0 row d-flex align-items-center chartbox flowchart" style="position: relative;">
                        @if(isset($beforedata[0]))
                            <div id="no1" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[0]}}<br><span>{{$beforedata[1]}}%</span></div></div>
                        @else
                            <div id="no1" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
                        @endif
                        @if(isset($beforedata[2]))
                            <div id="no2" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[2]}}<br><span>{{$beforedata[3]}}%</span></div></div>
                        @else
                            <div id="no2" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
                        @endif
                        @if(isset($beforedata[4]))
                            <div id="no3" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$beforedata[4]}}<br><span>{{$beforedata[5]}}%</span></div></div>
                        @else
                            <div id="no3" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
                        @endif
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r1 row1">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r2 row1">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r3 row1">
                        <div class="myarrow">
                        <!--<span class="arrow left"></span>-->
                        <span class="line"></span>
                        <!--<span class="arrow right"></span>-->
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow11 row1">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow12 row1"></div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow13 row1">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r1 last row1">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow12 last row1"></div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r3 last row1 ">
                        <div class="myarrow">
                        <!--<span class="arrow left"></span>-->
                        <span class="line"></span>
                        <!--<span class="arrow right"></span>-->
                        </div>
                    </div>
                    <div class="col-xs-2 col-md-2 col-2"></div>
                    <div id="centerbox" class="col-xs-7 col-md-7 col-7">
                        <div class="fchart"> {{ $product }} </div>
                    </div>
                    <div class="col-xs-3 col-md-3 col-3"></div>
                            <div class="col-xs-4 col-md-4 col-4 centerarrow r1 last row2">
                                <div class="myarrow">
                                    <span class="line"></span>
                                </div>
                            </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow12 last row2"></div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r3 last row2">
                        <div class="myarrow">
                        <!--<span class="arrow left"></span>-->
                        <span class="line"></span>
                        <!--<span class="arrow right"></span>-->
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow11 row2">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow12 row2"></div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow13 row2">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r1 row2">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r2 row2">
                        <div class="myarrow">
                        <span class="line"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 col-4 centerarrow r3 row2">
                        <div class="myarrow">
                        <!--<span class="arrow left"></span>-->
                        <span class="line"></span>
                        <!--<span class="arrow right"></span>-->
                        </div>
                    </div>
                    @if(isset($afterdata[0]))
                        <div id="no4" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[0]}}<br><span>{{$afterdata[1]}}%</span></div></div>
                    @else
                        <div id="no4" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
                    @endif
                    @if(isset($afterdata[2]))
                        <div id="no5" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[2]}}<br><span>{{$afterdata[3]}}%</span></div></div>
                    @else
                        <div id="no5" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
                    @endif
                    @if(isset($afterdata[4]))
                        <div id="no6" class="col-xs-4 col-md-4 col-4"><div class="fchart">{{$afterdata[4]}}<br><span>{{$afterdata[5]}}%</span></div></div>
                    @else
                        <div id="no6" class="col-xs-4 col-md-4 col-4"><div class="fchart">לא זמין<br><span>&nbsp;</span></div></div>
                    @endif
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-- Tab 3 Ends -->

        <!-- Tab 4 Starts -->
        <div id="tab4data" class="tabcontent">
            <div class="row match-height firstrow j-center mh-80">
                <div class="col-12">
                        
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-end">
                                <h4 class="text-bold-800 color-dblack">Competition comparison</h4>
                                <p class="font-medium-5 mb-0" data-toggle="tooltip" data-placement="left" data-trigger="hover" data-html="true" title="{{ $tootltips[0] }}"><i class="feather icon-alert-circle cursor-pointer color-lblack"></i></p>
                            </div>
                            <hr class="m-10 mb-2" />
                            <div class="card-content">
                                <div class="card-body pt-0 row d-flex align-items-center">
                                <div class="col-4 form-group">
                                        <fieldset>
                                                <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Brand</span>
                                                </div>

                                                <select class="form-control font-medium-3 dynamic" name="brand" id="brand" data-dependent="product" data-label="Choose a brand (optional)">
                                                    <option value="">Choose a brand (optional)</option>
                                                    @foreach($brands as $branddata)
                                                    <option value="{{ $branddata->brand }}">{{ $branddata->brand }}</option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </fieldset>
                                    </div>
                                    <div class="col-4 form-group">
                                        <fieldset>
                                                <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Product</span>
                                                </div>

                                                <select class="form-control font-medium-3" name="product" id="product" data-label="Choose a product (optional)">
                                                    <option value="">Choose a product (optional)</option>
                                                    @foreach($products as $productdata)
                                                    <option value="{{ $productdata->product }}">{{ $productdata->product }}</option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </fieldset>
                                    </div>
                                    
                                    <div class="col-4 form-group">
                                        <button type="submit" class="btn btn-outline-primary mb-1 col-12 font-medium-5 waves-effect waves-light"><i class="fa fa-plus-circle pad-rl0"></i> Competition comparison</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body pt-0 row d-flex align-items-center">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <th scope="col">&nbsp;</th>
                                                    <th scope="col">&nbsp;</th>
                                                    <th scope="col" colspan="5"><h2>Mentos sales during promotion</h2></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="bg-dgrey">
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>-203 </td>
                                                    <td>2+1</td>
                                                    <td>-152</td>
                                                    <td>-14.92</td>
                                                    <td>10</td>
                                                </tr>
                                                <tr>
                                                    <td class="bg-grey" rowspan="5"><h2>Orbit sales during promotion</h2></td>
                                                    <td>-152</td>
                                                    <td>154.0%</td>
                                                    <td class="bg-yellow"><strong>200.0%</strong></td>
                                                    <td class="bg-yellow"><strong>183.0%</strong></td>
                                                    <td>108.3%</td>
                                                    <td>181.0%</td>
                                                </tr>
                                                <tr>                                                
                                                    <td>-203</td>
                                                    <td>&nbsp;</td>
                                                    <td class="bg-yellow"><strong>235.0%</strong></td>
                                                    <td>117.1%</td>
                                                    <td>&nbsp;</td>
                                                    <td>134.7%</td>
                                                </tr>
                                                <tr>                                                
                                                    <td>-243</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>169.7%</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>                                                
                                                    <td>-202</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>155.2%</td>
                                                    <td><strong>169.0%</strong></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>                                                
                                                    <td>9.9</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class="bg-yellow"><strong>350.0%</strong></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>                                                
                                                    <td colspan="2">Orbit sales not during promtion</td>
                                                    <td class="bg-yellow"><strong>201.1%</strong></td>
                                                    <td>&nbsp;</td>
                                                    <td>165.0%</td>
                                                    <td>&nbsp;</td>
                                                    <td>133.4%</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>                            
                </div>
            </div>
        </div>
        <!-- Tab 4 Ends -->

        <!-- Tab 5 Starts -->
        <div id="tab5data" class="tabcontent">
            <div class="row match-height firstrow j-center mh-80">
                    <div class="col-4">
                        <img src="{{asset('images/img1.jpeg') }}" alt="Shelf Image" height="auto" width="100%" id="bigimg" /> 
                    </div>
                    <div class="col-8">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body pt-0 row d-flex align-items-center">
                                    <br /><br /><br /><h2>Planogram data table</h2>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Planogram image</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Product price</th>
                                                    <th scope="col">The impulse index</th>
                                                    <th scope="col">Purchase percentages</th>
                                                    <th scope="col">Sales</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img1.jpeg') }}"><img src="{{asset('images/img1.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>12/8/2021-11/9/2021</td>
                                                    <td>8.9 NIS</td>
                                                    <td>40% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-95% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-12% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr>   
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img2.jpeg') }}"><img src="{{asset('images/img2.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>10/6/2021-17/7/2021</td>
                                                    <td>8.9 NIS</td>
                                                    <td>55% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>+10% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-2.5% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr> 
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img3.jpeg') }}"><img src="{{asset('images/img3.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>10/6/2021-17/7/2021</td>
                                                    <td>7.5 NIS</td>
                                                    <td>70% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-2% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>+25% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr>   
                                                <tr>
                                                    <td><a href="#" class="openbigimg" data-src="{{asset('images/img4.jpeg') }}"><img src="{{asset('images/img4.jpeg') }}" alt="Shelf behavior" height="auto" width="64px" /> <i class="fa fa-external-link"></i> </a></td>
                                                    <td>10/6/2021-17/7/2021</td>
                                                    <td>8.9 NIS</td>
                                                    <td>55% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>+10% <img src="{{asset('images/ua.png') }}" alt="" height="auto" width="16px" /></td>
                                                    <td>-2.5% <img src="{{asset('images/da.png') }}" alt="" height="auto" width="16px" /></td>
                                                </tr>                                               
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>  
            </div>
        </div>
        <!-- Tab 5 Ends -->

        <!-- Tab 6 Starts -->
        <div id="tab6data" class="tabcontent">
        </div>
        <!-- Tab 6 Ends -->

        <!-- Tab 7 Starts -->
        <div id="tab7data" class="tabcontent">
            <video width="100%" height="100%" controls>
                <source src="{{asset('images/DRILL_Women_35_Emotion.mp4') }}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
        <!-- Tab 7 Ends -->
    </section>

  @else
		<!-- error 404 -->
<section class="row">
  <div class="col-xl-4 col-md-4 col-12 offset-4 d-flex justify-content-center">
    <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
      <div class="card-content">
        <div class="card-body text-center">
          <img src="{{ asset('images/pages/404.png') }}" class="img-fluid align-self-center" alt="branding logo">
          <h1 class="font-large-2 my-1">לא נמצאו מספיק נתונים!</h1>
          <p class="p-2">
			אין לנו מספיק נתונים כדי להציג תוצאות התואמות את הקריטריונים שלך. אנא לחץ על הלחצן למטה כדי לבחור קריטריונים אחרים.
          </p>
          <a class="btn btn-primary btn-lg mt-2" href="/search-information">חזרה לחיפוש</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- error 404 end -->
@endif
  {{-- Dashboard Ecommerce ends --}}
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/charts/chart.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/charts/echarts/echarts.min.js')) }}"></script>
		<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/pages/apexChart.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/modal/components-modal.js')) }}"></script>
@endsection
