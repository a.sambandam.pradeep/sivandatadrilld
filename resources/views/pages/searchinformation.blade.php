
@extends('layouts/fullLayoutMaster')

@section('title', 'Data Form')
@section('vendor-style')
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('content')

<style>
    .app-content.content{
        background: transparent linear-gradient(180deg, #8E2DE2 0%, #4A00E0 100%) 0% 0% no-repeat padding-box;
		min-height: 100vh;
    }

    </style>
<!-- Basic Vertical form layout section start -->
<section id="basic-vertical-layouts">
  <div class="row match-height">

      <div class="col-md-4 offset-md-4 col-12">
          <div class="card">
              <div class="card-content">
                  <div class="card-body">
                    <h4 class="text-bold-800 color-dblack font-medium-4">{{ $greetings }}, {{ $user->name }}</h4><a href="auth-logout"><i class="feather icon-power"></i> logout</a>
                    <hr />
                    <p class="text-bold-600 color-dblack font-medium-4">Filters:</p>
                      <form class="form form-vertical" action="{{ url('results-dashboard')}}" method="POST">
                          <div class="form-body">
                              <div class="row">
                                  <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">location_on</span> Region</span>
                                            </div>

											<select name="local" id="local" class="form-control form-control font-medium-3 dynamic" data-dependent="dp">
												 <option value="">Choose a city</option>
												 @foreach($regions as $region)
												 <option value="{{ $region->local }}">{{ $region->local }}</option>
												 @endforeach
											</select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">date_range</span> Date range</span>
                                            </div>

                                            <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="fromdate" id="fromdate" placeholder="From" />
                                            <input type='text' class="form-control pickadate dynamic" data-dependent="dp" name="todate" id="todate" placeholder="To" />
                                            </div>
                                        </fieldset>
                                  </div>
                                  <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">store</span> Department</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="dp" id="dp" data-dependent="category" data-label="Choose a department">
                                                <option value="">Choose a department</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Category</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="category" id="category" data-dependent="brand" data-label="Choose a category">
                                                <option value="">Choose a category</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Brand</span>
                                            </div>

                                            <select class="form-control font-medium-3 dynamic" name="brand" id="brand" data-dependent="product" data-label="Choose a brand (optional)">
                                                <option value="">Choose a brand (optional)</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                <div class="col-12 form-group">
                                    <fieldset>
                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-medium-3"><span class="material-icons pad-rl0">loyalty</span> Product</span>
                                            </div>

                                            <select class="form-control font-medium-3" name="product" id="product" data-label="Choose a product (optional)">
                                                <option value="">Choose a product (optional)</option>
                                            </select>
                                            </div>
                                        </fieldset>
                                </div>
                                    <div class="col-12">
										{{ csrf_field() }}
                                      <button type="submit" class="btn btn-primary mr-1 mb-1 col-12 btn-spurple font-medium-5">Search</button>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
<!-- // Basic Vertical form layout section end -->

@endsection

@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
@endsection
@section('page-script')
        <!-- Page js files -->
@endsection
