/*=========================================================================================
    File Name: dashboard-ecommerce.js
    Description: dashboard ecommerce page content with Apexchart Examples
    ----------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

//const { round } = require("lodash");

$(window).on("load", function () {

	var $primary = '#7367F0';
	var $success = '#28C76F';
	var $danger = '#EA5455';
	var $warning = '#FF9F43';
	var $info = '#00cfe8';
	var $primary_light = '#A9A2F6';
	var $danger_light = '#f29292';
	var $success_light = '#55DD92';
	var $warning_light = '#ffc085';
	var $info_light = '#1fcadb';
	var $strok_color = '#b9c3cd';
	var $label_color = '#e7e7e7';
	var $white = '#fff';


  // Session Chart
  // ----------------------------------
	$('.doughcharts').each(function(){
		var percentages = $(this).data('percentages');
		var percentagesarr = percentages.split(",");
		var percentagesarr = $.map(percentages.split(','), function(value){
			return parseInt(value, 10);
			// or return +value; which handles float values as well
		});
		var labels = $(this).data('labels');
		var labelsarr = labels.split(",");
		var colors = $(this).data('colors');
		var colorsarr = colors.split(",");
		var divid = $(this).attr('id');
		var totalval = $(this).data('totalval');
		var icon =$(this).data('icon');
		var labelcolors = $(this).data('labelcolors');
		var labelcolorsarr = labelcolors.split(",");
		var sessionChartoptions = {
			chart: {
				type: 'donut',
				width: '100%',
				fontFamily: 'Assistant, Helvetica, Arial, sans-serif',
				toolbar: {
					show: false
				}
			},
			dataLabels: {
				enabled: true,
				textAnchor: 'end',
				distributed: true,
				formatter: function (val) {
					var newval = Math.round(val);
					return newval + "%"
				},
				style: {
					fontSize: '10px',
					fontFamily: 'Helvetica, Arial, sans-serif',
					fontWeight: 'bold',
					colors: labelcolorsarr
				},
				dropShadow: {
					enabled: false,
				},
			},
			series: percentagesarr,
			legend: {
				show: false
			},
			labels: labelsarr,
			stroke: { width: 0 },
			colors: colorsarr,
			plotOptions: {
				pie: {
					offsetX: 10,
					offsetX: 10,
					customScale: 0.9,
					dataLabels: {
						offset: 20,
						minAngleToShowLabel: 10
					},
					donut: {
						labels: {
							show: true,
							name: {
									show: false
								},
							value: {
									show: true,
									fontFamily: 'Assistant',
									fontWeight: "800",
									color:'#4A4A4A',
									fontSize: "24px",
									formatter: function (val) {
										var newval = Math.round(val);
										return newval + "%"
									}
								},
								icon: {
									show: true,
									fontFamily: 'FontAwesome',
									fontWeight: "800",
									color:'#4A4A4A',
									fontSize: "24px",
									icon: icon
								},
								total: {
									show: true,
									showAlways: true,
									fontFamily: 'Assistant',
									fontWeight: "800",
									color:'#4A4A4A',
									fontSize: "35px",
									formatter: function (w) {
										  if(divid == 'chart1'){
												return w.globals.seriesTotals.reduce((a, b) => {
													return Math.round(totalval)
												}, 0)
											}else{
												return ''
											}
									}
								}
						}
					},
				}
			}
		}
		var sessionChart = new ApexCharts(
			document.querySelector("#"+divid),
			sessionChartoptions
			);
		sessionChart.render();

	});

	var $dark_green = '#F56323';
	var $green = '#FBC02D';
	var $light_green = '#7ED321';

	var $primary = '#7367F0';
	var $success = '#28C76F';
	var $danger = '#EA5455';
	var $warning = '#FF9F43';
	var $label_color = '#1E1E1E';
	var grid_line_color = '#dae1e7';
	var scatter_grid_color = '#f3f3f3';
	var $scatter_point_light = '#D1D4DB';
	var $scatter_point_dark = '#5175E0';
	var $white = '#fff';
	var $black = '#000';

	var themeColors = [$light_green, $green, $dark_green];
	// Bar Chart
	// ------------------------------------------

	//Get the context of the Chart canvas element we want to select
	var barChartctx = $("#bar-chart");
	var barChartctxbar = document.getElementById("bar-chart").getContext("2d");
	// barChartctx.height = 50;
	var barvalues = barChartctx.data('values');
	var barvaluesarr = barvalues.split(",");
	// Chart Options
	var barchartOptions = {
	// Elements options apply to all of the options unless overridden in a dataset
	// In this case, we are setting the border of each bar to be 2px wide
		elements: {
			rectangle: {
				borderWidth: 2,
				borderSkipped: 'left'
			}
		},
		responsive: true,
		maintainAspectRatio: false,
		responsiveAnimationDuration: 500,
		legend: { display: false },
		tooltips: {
            callbacks: {
				label: function(tooltipItem) {
					return "" + Number(tooltipItem.yLabel) + "%";
				}
			}
		},
		hover: {
			animationDuration: 1
		},
		animation: {
			duration: 2000,
			onComplete: function () {
			var chartInstance = this.chart;
			var ctx = chartInstance.ctx;
			var height = chartInstance.controller.boxes[0].bottom;
			ctx.textAlign = "center";
			Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
			  var meta = chartInstance.controller.getDatasetMeta(i);
			  Chart.helpers.each(meta.data.forEach(function (bar, index) {
				ctx.fillStyle = "#ffffff";
				ctx.fillText(dataset.data[index]+"%", bar._model.x, bar._model.y+20);
			  }),this)
			}),this);
		  }
		},
		scales: {
		xAxes: [{
			display: true,
			gridLines: {
				display: false
			},
			scaleLabel: {
				display: true,
			}
		}],
		yAxes: [{
			display: true,
			gridLines: {
				color: grid_line_color,
			},
			scaleLabel: {
				display: true,
			},
			ticks: {
				min: 0,
     			max: 100,
				stepSize: 20,
				callback: function(value, index, values) {
					return  value + '%';
				}
			},
		}],
		},
	title: {
	display: true,
	text: ''
	},

	};

	// Chart Data
	var barchartData = {
		labels: ["Product", "Brand", "Category"],
		datasets: [{
			label: "IMPULSE BUYING מדד ה",
			data: barvaluesarr,
			backgroundColor: themeColors,
			borderColor: "transparent"
		}]
	};

	var barChartconfig = {
		type: 'bar',
		options: barchartOptions,
		data: barchartData
	};

	// Create the chart
	var barChart = new Chart(barChartctxbar, barChartconfig);

	var ctx = document.getElementById("groupChart").getContext("2d");

	var data = {
		labels: ["5-10 Years", "11-20 Years", "21-30 Years", "31-40 Years", "41-50 Years", "51-80 Years"],
		datasets: [{
		  label: "Male",
		  backgroundColor: "#748789",
		  data: [3, 7, 4, 6, 8, 5]
		}, {
		  label: "Female",
		  backgroundColor: "#58b6c0",
		  data: [4, 3, 5, 1, 3, 8]
		}]
	  };
	  
	  var myBarChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		grouped: true,
		options: {
		  barValueSpacing: 100,
		  scales: {
			yAxes: [{
			  ticks: {
				beginAtZero: true,
				min: 0,
			  }
			}]
		  }
		}
	  });
	  

	  $('.tabref').on('click', function(e){
		var activetab = $(this).data('tabid');
		$('.tabcontent').fadeOut();
		$('#tab'+activetab+'data').fadeIn();
		$('.menu-toggle').trigger('click');
		e.preventDefault();
	  });
	  $('.openbigimg').on('click', function(e){
		var src = $(this).data('src');
		$('#bigimg').attr('src',src);
		e.preventDefault();
	  });

});
