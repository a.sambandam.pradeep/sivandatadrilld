/*=========================================================================================
    File Name: dropzone.js
    Description: dropzone
    --------------------------------------------------------------------------------------
    Item name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


/********************************************
*               Accepted Files              *
********************************************/

Dropzone.options.dpAcceptFiles = {
	paramName: "file", // The name that will be used to transfer the file
	maxFilesize: 50, // MB
	acceptedFiles : ".xls,.xlsx,.csv",
	init: function(){
		this.on("error", function(file, errorMessage) {
			console.log(errorMessage);
		});
				this.on("success", function (file) {
            window.location.replace("/");
        });
	},
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}
