/*!
Waypoints - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
!function(){"use strict";function t(o){if(!o)throw new Error("No options passed to Waypoint constructor");if(!o.element)throw new Error("No element option passed to Waypoint constructor");if(!o.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,o),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=o.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var o in i)e.push(i[o]);for(var n=0,r=e.length;r>n;n++)e[n][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.Context.refreshAll();for(var e in i)i[e].enabled=!0;return this},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,o[t.waypointContextKey]=this,i+=1,n.windowContext||(n.windowContext=!0,n.windowContext=new e(window)),this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,o={},n=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical),i=this.element==this.element.window;t&&e&&!i&&(this.adapter.off(".waypoints"),delete o[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,n.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||n.isTouch)&&(e.didScroll=!0,n.requestAnimationFrame(t))})},e.prototype.handleResize=function(){n.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var o=e[i],n=o.newScroll>o.oldScroll,r=n?o.forward:o.backward;for(var s in this.waypoints[i]){var a=this.waypoints[i][s];if(null!==a.triggerPoint){var l=o.oldScroll<a.triggerPoint,h=o.newScroll>=a.triggerPoint,p=l&&h,u=!l&&!h;(p||u)&&(a.queueTrigger(r),t[a.group.id]=a.group)}}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var o=0,n=t.length;n>o;o++)t[o].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var a in this.waypoints[r]){var l,h,p,u,c,d=this.waypoints[r][a],f=d.options.offset,w=d.triggerPoint,y=0,g=null==w;d.element!==d.element.window&&(y=d.adapter.offset()[s.offsetProp]),"function"==typeof f?f=f.apply(d):"string"==typeof f&&(f=parseFloat(f),d.options.offset.indexOf("%")>-1&&(f=Math.ceil(s.contextDimension*f/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=Math.floor(y+l-f),h=w<s.oldScroll,p=d.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!g&&u?(d.queueTrigger(s.backward),o[d.group.id]=d.group):!g&&c?(d.queueTrigger(s.forward),o[d.group.id]=d.group):g&&s.oldScroll>=d.triggerPoint&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame(function(){for(var t in o)o[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in o)o[t].refresh()},e.findByElement=function(t){return o[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},n.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},n.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),o[this.axis][this.name]=this}var o={vertical:{},horizontal:{}},n=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var o=this.triggerQueues[i],n="up"===i||"left"===i;o.sort(n?e:t);for(var r=0,s=o.length;s>r;r+=1){var a=o[r];(a.options.continuous||r===o.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints),o=i===this.waypoints.length-1;return o?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return o[t.axis][t.name]||new i(t)},n.Group=i}(),function(){"use strict";function t(t){this.$element=e(t)}var e=window.jQuery,i=window.Waypoint;e.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(e,i){t.prototype[i]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[i].apply(this.$element,t)}}),e.each(["extend","inArray","isEmptyObject"],function(i,o){t[o]=e[o]}),i.adapters.push({name:"jquery",Adapter:t}),i.Adapter=t}(),function(){"use strict";function t(t){return function(){var i=[],o=arguments[0];return t.isFunction(arguments[0])&&(o=t.extend({},arguments[1]),o.handler=arguments[0]),this.each(function(){var n=t.extend({},o,{element:this});"string"==typeof n.context&&(n.context=t(this).closest(n.context)[0]),i.push(new e(n))}),i}}var e=window.Waypoint;window.jQuery&&(window.jQuery.fn.waypoint=t(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=t(window.Zepto))}();

/*=========================================================================================
  File Name: app.js
  Description: Template related app JS.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: hhttp://www.themeforest.net/user/pixinvent
==========================================================================================*/

(function (window, document, $) {
  "use strict";
  var $html = $("html");
  var $body = $("body");
  var $danger = "#ea5455";
  var $primary = "#7367f0";
  var $textcolor = "#4e5154";

  $(window).on("load", function () {
    var rtl;
    var compactMenu = false; // Set it to true, if you want default menu to be compact

    if ($body.hasClass("menu-collapsed")) {
      compactMenu = true;
    }

    if ($("html").data("textdirection") == "rtl") {
      rtl = true;
    }

    setTimeout(function () {
      $html.removeClass("loading").addClass("loaded");
    }, 1200);

    $.app.menu.init(compactMenu);

    // Navigation configurations
    var config = {
      speed: 300 // set speed to expand / collpase menu
    };
    if ($.app.nav.initialized === false) {
      $.app.nav.init(config);
    }

    Unison.on("change", function (bp) {
      $.app.menu.change();
    });

    // Tooltip Initialization
    $('[data-toggle="tooltip"]').tooltip({
      container: "body"
    });

    // Top Navbars - Hide on Scroll
    if ($(".navbar-hide-on-scroll").length > 0) {
      $(".navbar-hide-on-scroll.fixed-top").headroom({
        offset: 205,
        tolerance: 5,
        classes: {
          // when element is initialised
          initial: "headroom",
          // when scrolling up
          pinned: "headroom--pinned-top",
          // when scrolling down
          unpinned: "headroom--unpinned-top"
        }
      });
      // Bottom Navbars - Hide on Scroll
      $(".navbar-hide-on-scroll.fixed-bottom").headroom({
        offset: 205,
        tolerance: 5,
        classes: {
          // when element is initialised
          initial: "headroom",
          // when scrolling up
          pinned: "headroom--pinned-bottom",
          // when scrolling down
          unpinned: "headroom--unpinned-bottom"
        }
      });
    }

    // Collapsible Card
    $('a[data-action="collapse"]').on("click", function (e) {
      e.preventDefault();
      $(this)
        .closest(".card")
        .children(".card-content")
        .collapse("toggle");
      // Adding bottom padding on card collapse
      $(this)
        .closest(".card")
        .children(".card-header")
        .css("padding-bottom", "1.5rem");
      $(this)
        .closest(".card")
        .find('[data-action="collapse"]')
        .toggleClass("rotate");
    });

    // Toggle fullscreen
    $('a[data-action="expand"]').on("click", function (e) {
      e.preventDefault();
      $(this)
        .closest(".card")
        .find('[data-action="expand"] i')
        .toggleClass("icon-maximize icon-minimize");
      $(this)
        .closest(".card")
        .toggleClass("card-fullscreen");
    });

    //  Notifications & messages scrollable
    $(".scrollable-container").each(function () {
      var scrollable_container = new PerfectScrollbar($(this)[0], {
        wheelPropagation: false
      });
    });

    // Reload Card
    $('a[data-action="reload"]').on("click", function () {
      var block_ele = $(this)
        .closest(".card")
        .find(".card-content");
      var reloadActionOverlay;
      if ($body.hasClass("dark-layout")) {
        var reloadActionOverlay = "#10163a";
      } else {
        var reloadActionOverlay = "#fff";
      }
      // Block Element
      block_ele.block({
        message: '<div class="feather icon-refresh-cw icon-spin font-medium-2 text-primary"></div>',
        timeout: 2000, //unblock after 2 seconds
        overlayCSS: {
          backgroundColor: reloadActionOverlay,
          cursor: "wait"
        },
        css: {
          border: 0,
          padding: 0,
          backgroundColor: "none"
        }
      });
    });

    // Close Card
    $('a[data-action="close"]').on("click", function () {
      $(this).closest(".card").removeClass().slideUp("fast");
    });

    // Match the height of each card in a row
    setTimeout(function () {
      $(".row.match-height").each(function () {
        $(this).find(".card").not(".card .card").matchHeight(); // Not .card .card prevents collapsible cards from taking height
      });
    }, 500);

    $('.card .heading-elements a[data-action="collapse"]').on(
      "click",
      function () {
        var $this = $(this),
          card = $this.closest(".card");
        var cardHeight;

        if (parseInt(card[0].style.height, 10) > 0) {
          cardHeight = card.css("height");
          card.css("height", "").attr("data-height", cardHeight);
        } else {
          if (card.data("height")) {
            cardHeight = card.data("height");
            card.css("height", cardHeight).attr("data-height", "");
          }
        }
      }
    );

    // Add sidebar group active class to active menu
    $(".main-menu-content").find("li.active").parents("li").addClass("sidebar-group-active");

    // Add open class to parent list item if subitem is active except compact menu
    var menuType = $body.data("menu");
    if (menuType != "horizontal-menu" && compactMenu === false) {
      $(".main-menu-content").find("li.active").parents("li").addClass("open");
    }
    if (menuType == "horizontal-menu") {
      $(".main-menu-content").find("li.active").parents("li:not(.nav-item)").addClass("open");
      $(".main-menu-content").find('li.active').closest('li.nav-item').addClass('sidebar-group-active open');
      // $(".main-menu-content")
      //   .find("li.active")
      //   .parents("li")
      //   .addClass("active");
    }

    //card heading actions buttons small screen support
    $(".heading-elements-toggle").on("click", function () {
      $(this)
        .next(".heading-elements")
        .toggleClass("visible");
    });

    //  Dynamic height for the chartjs div for the chart animations to work
    var chartjsDiv = $(".chartjs"),
      canvasHeight = chartjsDiv.children("canvas").attr("height"),
      mainMenu = $(".main-menu");
    chartjsDiv.css("height", canvasHeight);

    if ($body.hasClass("boxed-layout")) {
      if ($body.hasClass("vertical-overlay-menu")) {
        var menuWidth = mainMenu.width();
        var contentPosition = $(".app-content").position().left;
        var menuPositionAdjust = contentPosition - menuWidth;
        if ($body.hasClass("menu-flipped")) {
          mainMenu.css("right", menuPositionAdjust + "px");
        } else {
          mainMenu.css("left", menuPositionAdjust + "px");
        }
      }
    }

    //Custom File Input
    $(".custom-file input").change(function (e) {
      $(this)
        .next(".custom-file-label")
        .html(e.target.files[0].name);
    });

    /* Text Area Counter Set Start */

    $(".char-textarea").on("keyup", function (event) {
      checkTextAreaMaxLength(this, event);
      // to later change text color in dark layout
      $(this).addClass("active");
    });

    /*
    Checks the MaxLength of the Textarea
    -----------------------------------------------------
    @prerequisite:  textBox = textarea dom element
            e = textarea event
                    length = Max length of characters
    */
    function checkTextAreaMaxLength(textBox, e) {
      var maxLength = parseInt($(textBox).data("length")),
        counterValue = $(".counter-value"),
        charTextarea = $(".char-textarea");

      if (!checkSpecialKeys(e)) {
        if (textBox.value.length < maxLength - 1)
          textBox.value = textBox.value.substring(0, maxLength);
      }
      $(".char-count").html(textBox.value.length);

      if (textBox.value.length > maxLength) {
        counterValue.css("background-color", $danger);
        charTextarea.css("color", $danger);
        // to change text color after limit is maxedout out
        charTextarea.addClass("max-limit");
      } else {
        counterValue.css("background-color", $primary);
        charTextarea.css("color", $textcolor);
        charTextarea.removeClass("max-limit");
      }

      return true;
    }
    /*
    Checks if the keyCode pressed is inside special chars
    -------------------------------------------------------
    @prerequisite:  e = e.keyCode object for the key pressed
    */
    function checkSpecialKeys(e) {
      if (
        e.keyCode != 8 &&
        e.keyCode != 46 &&
        e.keyCode != 37 &&
        e.keyCode != 38 &&
        e.keyCode != 39 &&
        e.keyCode != 40
      )
        return false;
      else return true;
    }

    $(".content-overlay").on("click", function () {
      $(".search-list").removeClass("show");
      $(".app-content").removeClass("show-overlay");
      $(".bookmark-wrapper .bookmark-input").removeClass("show");
    });

    // To show shadow in main menu when menu scrolls
    var container = document.getElementsByClassName("main-menu-content");
    if (container.length > 0) {
      container[0].addEventListener("ps-scroll-y", function () {
        if (
          $(this)
          .find(".ps__thumb-y")
          .position().top > 0
        ) {
          $(".shadow-bottom").css("display", "block");
        } else {
          $(".shadow-bottom").css("display", "none");
        }
      });
    }
  });

  // Hide overlay menu on content overlay click on small screens
  $(document).on("click", ".sidenav-overlay", function (e) {
    // Hide menu
    //$.app.menu.hide();
    $('.menu-toggle').trigger('click');
    return false;
  });

  // Execute below code only if we find hammer js for touch swipe feature on small screen
  if (typeof Hammer !== "undefined") {
    // Swipe menu gesture
    var swipeInElement = document.querySelector(".drag-target");

    if ($(swipeInElement).length > 0) {
      var swipeInMenu = new Hammer(swipeInElement);

      swipeInMenu.on("panright", function (ev) {
        if ($body.hasClass("vertical-overlay-menu")) {
          $.app.menu.open();
          return false;
        }
      });
    }

    // menu swipe out gesture
    setTimeout(function () {
      var swipeOutElement = document.querySelector(".main-menu");
      var swipeOutMenu;

      if ($(swipeOutElement).length > 0) {
        swipeOutMenu = new Hammer(swipeOutElement);

        swipeOutMenu.get("pan").set({
          direction: Hammer.DIRECTION_ALL,
          threshold: 100
        });

        swipeOutMenu.on("panleft", function (ev) {
          if ($body.hasClass("vertical-overlay-menu")) {
            $.app.menu.hide();
            return false;
          }
        });
      }
    }, 300);

    // menu overlay swipe out gestrue
    var swipeOutOverlayElement = document.querySelector(".sidenav-overlay");

    if ($(swipeOutOverlayElement).length > 0) {
      var swipeOutOverlayMenu = new Hammer(swipeOutOverlayElement);

      swipeOutOverlayMenu.on("panleft", function (ev) {
        if ($body.hasClass("vertical-overlay-menu")) {
          $.app.menu.hide();
          return false;
        }
      });
    }
  }

  $(document).on("click", ".menu-toggle, .modern-nav-toggle", function (e) {  
      
      $('.header-navbar .navbar-container ul.nav li a.menu-toggle i').toggleClass('icon-x');
    e.preventDefault();

    // Toggle menu
    $.app.menu.toggle();
	
    setTimeout(function () {
      $(window).trigger("resize");
    }, 200);
/*
    if ($("#collapse-sidebar-switch").length > 0) {
      setTimeout(function () {
        if ($body.hasClass("menu-expanded") || $body.hasClass("menu-open")) {
          $("#collapse-sidebar-switch").prop("checked", false);
        } else {
          $("#collapse-sidebar-switch").prop("checked", true);
        }
      }, 50);
    }*/

    // Hides dropdown on click of menu toggle
    // $('[data-toggle="dropdown"]').dropdown('hide');

    // Hides collapse dropdown on click of menu toggle
    if (
      $(".vertical-overlay-menu .navbar-with-menu .navbar-container .navbar-collapse").hasClass("show")
    ) {
      $(".vertical-overlay-menu .navbar-with-menu .navbar-container .navbar-collapse").removeClass("show");
    }

    return false;
  });

  // Add Children Class
  $(".navigation")
    .find("li")
    .has("ul")
    .addClass("has-sub");

  $(".carousel").carousel({
    interval: 2000
  });

  // Page full screen
  $(".nav-link-expand").on("click", function (e) {
    if (typeof screenfull != "undefined") {
      if (screenfull.isEnabled) {
        screenfull.toggle();
      }
    }
  });
  if (typeof screenfull != "undefined") {
    if (screenfull.isEnabled) {
      $(document).on(screenfull.raw.fullscreenchange, function () {
        if (screenfull.isFullscreen) {
          $(".nav-link-expand")
            .find("i")
            .toggleClass("icon-minimize icon-maximize");
          $("html").addClass("full-screen");
        } else {
          $(".nav-link-expand")
            .find("i")
            .toggleClass("icon-maximize icon-minimize");
          $("html").removeClass("full-screen");
        }
      });
    }
  }
  $(document).ready(function () {
    /**********************************
     *   Form Wizard Step Icon
     **********************************/
    $(".step-icon").each(function () {
      var $this = $(this);
      if ($this.siblings("span.step").length > 0) {
        $this.siblings("span.step").empty();
        $(this).appendTo($(this).siblings("span.step"));
      }
    });
	$('.progress-bar').waypoint(function() {
		$('.progress-bar').css({
			animation: "animate-positive 5s",
			opacity: "1"
		});
  }, { offset: '75%' });
  if( $('.dynamic').length){
    var from_$input = $('#fromdate').pickadate({
        format: 'dd/mm/yyyy',
        formatSubmit: 'yyyy-mm-dd',
		max: true,
        selectYears: true,
        selectMonths: true
      }),
    from_picker = from_$input.pickadate('picker');

    var to_$input = $('#todate').pickadate({
      format: 'dd/mm/yyyy',
      formatSubmit: 'yyyy-mm-dd',
      selectYears: true,
      selectMonths: true
    }),
    to_picker = to_$input.pickadate('picker');

    if (from_picker.get('value')) {
        var checkIN = new Date($('#fromdate').val());
        checkIN.setDate(checkIN.getDate() + 1);
        var dateDateN = checkIN.getDate();
        var dateMonthN = checkIN.getMonth() + 1; //January is 0!
        var dateYearN = checkIN.getFullYear();
        to_picker.set('min', new Date(dateYearN, dateMonthN - 1, dateDateN + 1));
    }

    if (to_picker.get('value')) {
        //from_picker.set('max', to_picker.get('select'))
    }
    from_picker.on('set', function (event) {
        if (event.select) {
          // alert(from_picker.get('select'));
        var checkIN = new Date($('#fromdate').val());
        checkIN.setDate(checkIN.getDate() + 1);
        var dateDateN = checkIN.getDate();
        var dateMonthN = checkIN.getMonth() + 1; //January is 0!
        var dateYearN = checkIN.getFullYear();
        to_picker.set('min', new Date(dateYearN, dateMonthN - 1, dateDateN + 1));
        } else if ('clear' in event) {
            to_picker.set('min', false)
        }
    })
    $('.resetbtn').on('click',function(e){
      e.stopPropagation();
      document.getElementById('filterform').reset();
      document.getElementById('fromdate').value = '';
      document.getElementById('todate').value = '';

      $('#filterform select.form-control').each(function(){
        $(this).removeData('value');
        $(this).data( "value", "" );
        $(this).removeAttr( "data-value" );
        $(this).get(0).selectedIndex = 1;
        $(this).val('');
        if(!$(this).hasClass('local')){
          $(this).find('option').not(':first').remove();
        }
      });
      e.preventDefault();
    });
    $('.dynamic').change(function(){
      var fromdate = $('#fromdate').val();
      var todate = $('#todate').val();
      if($(this).val() != '' && fromdate != '' && todate != '' ){
        var select = $(this).attr("id");
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var local = $('#local').val();
        var dp = $('#dp').val();
        var category = $('#category').val();
        var brand = $('#brand').val();
        if(select == 'todate'){
          select = 'local';
          value = local;
        }else{
          var value = $(this).val();
        }  
        $.ajax({
          url:"/get-dps",
          method:"POST",
          data:{select:select, value:value, _token:_token, dependent:dependent, local:local, fromdate:fromdate, todate:todate, dp:dp, category:category, brand:brand },
          success:function(result){
            var firstoption = $('#'+dependent).data('label');
            $('#'+dependent).html('<option value="">'+firstoption+'</option>'+result);
            if($('#initload').val() == 'no'){
              var currval = $('#'+dependent).data('value');
              if(dependent == 'product'){
                $('#initload').val('yes');
              }
            }
            
            $('#'+dependent).val(currval).trigger('change');
            if(dependent == 'dp'){
              $('#category').find('option').remove().end().append('<option value="">'+$('#category').data('label')+'</option>');
              $('#brand').find('option').remove().end().append('<option value="">'+$('#brand').data('label')+'</option>');
              $('#product').find('option').remove().end().append('<option value="">'+$('#product').data('label')+'</option>');
            }
            if(dependent == 'category'){							
              $('#brand').find('option').remove().end().append('<option value="">'+$('#brand').data('label')+'</option>');
              $('#product').find('option').remove().end().append('<option value="">'+$('#product').data('label')+'</option>');
            }
            if(dependent == 'brand'){														
              $('#product').find('option').remove().end().append('<option value="">'+$('#product').data('label')+'</option>');
            }
          }
        })
      }
    });
    if($('#local').val() != ''){
      $('#local').trigger('change');
    }
  }
  });

  // Update manual scroller when window is resized
  $(window).resize(function () {
    $.app.menu.manualScroller.updateHeight();
  });

  $("#sidebar-page-navigation").on("click", "a.nav-link", function (e) {
    e.preventDefault();
    e.stopPropagation();
    var $this = $(this),
      href = $this.attr("href");
    var offset = $(href).offset();
    var scrollto = offset.top - 80; // minus fixed header height
    $("html, body").animate({
        scrollTop: scrollto
      },
      0
    );
    setTimeout(function () {
      $this
        .parent(".nav-item")
        .siblings(".nav-item")
        .children(".nav-link")
        .removeClass("active");
      $this.addClass("active");
    }, 100);
  });

  // main menu internationalization

  // init i18n and load language file
  i18next.use(window.i18nextXHRBackend).init({
      debug: false,
      fallbackLng: "en",
      backend: {
        loadPath: "data/locales/{{lng}}.json"
      },
      returnObjects: true
    },
    function (err, t) {
      // resources have been loaded
      jqueryI18next.init(i18next, $);
    }
  );

  // change language according to data-language of dropdown item
  $(".dropdown-language .dropdown-item").on("click", function () {
    var $this = $(this);
    $this.siblings(".selected").removeClass("selected");
    $this.addClass("selected");
    var selectedLang = $this.text();
    var selectedFlag = $this.find(".flag-icon").attr("class");
    $("#dropdown-flag .selected-language").text(selectedLang);
    $("#dropdown-flag .flag-icon")
      .removeClass()
      .addClass(selectedFlag);
    var currentLanguage = $this.data("language");
    i18next.changeLanguage(currentLanguage, function (err, t) {
      $(".main-menu, .horizontal-menu-wrapper").localize();
    });
  });

  /********************* Bookmark & Search ***********************/
  // This variable is used for mouseenter and mouseleave events of search list
  var $filename = $(".search-input input").data("search"),
    bookmarkWrapper = $(".bookmark-wrapper"),
    bookmarkStar = $(".bookmark-wrapper .bookmark-star"),
    bookmarkInput = $(".bookmark-wrapper .bookmark-input"),
    navLinkSearch = $(".nav-link-search"),
    searchInput = $(".search-input"),
    searchInputInputfield = $(".search-input input"),
    searchList = $(".search-input .search-list"),
    appContent = $(".app-content"),
    bookmarkSearchList = $(".bookmark-input .search-list");

  // Bookmark icon click
  bookmarkStar.on("click", function (e) {
    e.stopPropagation();
    bookmarkInput.toggleClass("show");
    bookmarkInput.find("input").val("");
    bookmarkInput.find("input").blur();
    bookmarkInput.find("input").focus();
    bookmarkWrapper.find(".search-list").addClass("show");

    var arrList = $("ul.nav.navbar-nav.bookmark-icons li"),
      $arrList = "",
      $activeItemClass = "";

    $("ul.search-list li").remove();

    for (var i = 0; i < arrList.length; i++) {
      if (i === 0) {
        $activeItemClass = "current_item";
      } else {
        $activeItemClass = "";
      }
      $arrList +=
        '<li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer ' +
        $activeItemClass +
        '">' +
        '<a class="d-flex align-items-center justify-content-between w-100" href=' +
        arrList[i].firstChild.href +
        ">" +
        '<div class="d-flex justify-content-start align-items-center">' +
        '<span class="mr-75 ' +
        arrList[i].firstChild.firstChild.className +
        '"  data-icon="' +
        arrList[i].firstChild.firstChild.className +
        '"></span>' +
        "<span>" +
        arrList[i].firstChild.dataset.originalTitle +
        "</span>" +
        "</div>" +
        '<span class="float-right bookmark-icon feather icon-star warning"></span>' +
        "</a>" +
        "</li>";
    }
    $("ul.search-list").append($arrList);
  });

  // Navigation Search area Open
  navLinkSearch.on("click", function () {
    var $this = $(this);
    var searchInput = $(this).parent(".nav-search").find(".search-input");
    searchInput.addClass("open");
    searchInputInputfield.focus();
    searchList.find("li").remove();
    bookmarkInput.removeClass("show");
  });

  // Navigation Search area Close
  $(".search-input-close i").on("click", function () {
    var $this = $(this),
      searchInput = $(this).closest(".search-input");
    if (searchInput.hasClass("open")) {
      searchInput.removeClass("open");
      searchInputInputfield.val("");
      searchInputInputfield.blur();
      searchList.removeClass("show");
      appContent.removeClass("show-overlay");
    }
  });

  // Filter
  if ($('.search-list-main').length) {
    var searchListMain = new PerfectScrollbar(".search-list-main", {
      wheelPropagation: false
    });
  }
  if ($('.search-list-bookmark').length) {
    var searchListBookmark = new PerfectScrollbar(".search-list-bookmark", {
      wheelPropagation: false
    });
  }
  // update Perfect Scrollbar on hover
  $(".search-list-main").mouseenter(function () {
    searchListMain.update();
  });

  searchInputInputfield.on("keyup", function (e) {
    $(this).closest(".search-list").addClass("show");
    if (e.keyCode !== 38 && e.keyCode !== 40 && e.keyCode !== 13) {
      if (e.keyCode == 27) {
        appContent.removeClass("show-overlay");
        bookmarkInput.find("input").val("");
        bookmarkInput.find("input").blur();
        searchInputInputfield.val("");
        searchInputInputfield.blur();
        searchInput.removeClass("open");
        if (searchInput.hasClass("show")) {
          $(this).removeClass("show");
          searchInput.removeClass("show");
        }
      }

      // Define variables
      var value = $(this).val().toLowerCase(), //get values of input on keyup
        activeClass = "",
        bookmark = false,
        liList = $("ul.search-list li"); // get all the list items of the search
      liList.remove();
      // To check if current is bookmark input
      if (
        $(this)
        .parent()
        .hasClass("bookmark-input")
      ) {
        bookmark = true;
      }

      // If input value is blank
      if (value != "") {
        appContent.addClass("show-overlay");

        // condition for bookmark and search input click
        if (bookmarkInput.focus()) {
          bookmarkSearchList.addClass("show");
        } else {
          searchList.addClass("show");
          bookmarkSearchList.removeClass("show");
        }
        if (bookmark === false) {
          searchList.addClass("show");
          bookmarkSearchList.removeClass("show");
        }

        var $startList = "",
          $otherList = "",
          $htmlList = "",
          $bookmarkhtmlList = "",
          $pageList = '<li class=" d-flex align-items-center">' +
          '<a href="#" class="pb-25">' +
          '<h6 class="text-primary mb-0">Pages</h6>' +
          '</a>' +
          '</li>',
          $activeItemClass = "",
          $bookmarkIcon = "",
          $defaultList = "",
          a = 0;

        // getting json data from file for search results
        $.getJSON("data/" + $filename + ".json", function (
          data
        ) {
          for (var i = 0; i < data.listItems.length; i++) {
            // if current is bookmark then give class to star icon
            if (bookmark === true) {
              activeClass = ""; // resetting active bookmark class
              var arrList = $("ul.nav.navbar-nav.bookmark-icons li"),
                $arrList = "";
              // Loop to check if current seach value match with the bookmarks already there in navbar
              for (var j = 0; j < arrList.length; j++) {
                if (
                  data.listItems[i].name ===
                  arrList[j].firstChild.dataset.originalTitle
                ) {
                  activeClass = " warning";
                  break;
                } else {
                  activeClass = "";
                }
              }
              $bookmarkIcon =
                '<span class="float-right bookmark-icon feather icon-star' +
                activeClass +
                '"></span>';
            }
            // Search list item start with entered letters and create list
            if (
              data.listItems[i].name.toLowerCase().indexOf(value) == 0 &&
              a < 5
            ) {
              if (a === 0) {
                $activeItemClass = "current_item";
              } else {
                $activeItemClass = "";
              }
              $startList +=
                '<li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer ' +
                $activeItemClass +
                '">' +
                '<a class="d-flex align-items-center justify-content-between w-100" href=' +
                data.listItems[i].url +
                ">" +
                '<div class="d-flex justify-content-start align-items-center">' +
                '<span class="mr-75 ' +
                data.listItems[i].icon +
                '" data-icon="' +
                data.listItems[i].icon +
                '"></span>' +
                "<span>" +
                data.listItems[i].name +
                "</span>" +
                "</div>" +
                $bookmarkIcon +
                "</a>" +
                "</li>";
              a++;
            }
          }
          for (var i = 0; i < data.listItems.length; i++) {
            if (bookmark === true) {
              activeClass = ""; // resetting active bookmark class
              var arrList = $("ul.nav.navbar-nav.bookmark-icons li"),
                $arrList = "";
              // Loop to check if current seach value match with the bookmarks already there in navbar
              for (var j = 0; j < arrList.length; j++) {
                if (
                  data.listItems[i].name ===
                  arrList[j].firstChild.dataset.originalTitle
                ) {
                  activeClass = " warning";
                } else {
                  activeClass = "";
                }
              }
              $bookmarkIcon =
                '<span class="float-right bookmark-icon feather icon-star' +
                activeClass +
                '"></span>';
            }
            // Search list item not start with letters and create list
            if (
              !(data.listItems[i].name.toLowerCase().indexOf(value) == 0) &&
              data.listItems[i].name.toLowerCase().indexOf(value) > -1 &&
              a < 5
            ) {
              if (a === 0) {
                $activeItemClass = "current_item";
              } else {
                $activeItemClass = "";
              }
              $otherList +=
                '<li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer ' +
                $activeItemClass +
                '">' +
                '<a class="d-flex align-items-center justify-content-between w-100" href=' +
                data.listItems[i].url +
                ">" +
                '<div class="d-flex justify-content-start align-items-center">' +
                '<span class="mr-75 ' +
                data.listItems[i].icon +
                '" data-icon="' +
                data.listItems[i].icon +
                '"></span>' +
                "<span>" +
                data.listItems[i].name +
                "</span>" +
                "</div>" +
                $bookmarkIcon +
                "</a>" +
                "</li>";
              a++;
            }
          }
          $defaultList = $(".main-search-list-defaultlist").html();
          if ($startList == "" && $otherList == "") {
            $otherList = $(".main-search-list-defaultlist-other-list").html();
          }
          // concatinating startlist, otherlist, defalutlist with pagelist
          $htmlList = $pageList.concat($startList, $otherList, $defaultList);
          $("ul.search-list").html($htmlList);
          // concatinating otherlist with startlist
          $bookmarkhtmlList = $startList.concat($otherList);
          $("ul.search-list-bookmark").html($bookmarkhtmlList);
        });
      } else {
        if (bookmark === true) {
          var arrList = $("ul.nav.navbar-nav.bookmark-iconss li"),
            $arrList = "";
          for (var i = 0; i < arrList.length; i++) {
            if (i === 0) {
              $activeItemClass = "current_item";
            } else {
              $activeItemClass = "";
            }
            $arrList +=
              '<li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer">' +
              '<a class="d-flex align-items-center justify-content-between w-100" href=' +
              arrList[i].firstChild.href +
              ">" +
              '<div class="d-flex justify-content-start align-items-center">' +
              '<span class="mr-75 ' +
              arrList[i].firstChild.firstChild.className +
              '"  data-icon="' +
              arrList[i].firstChild.firstChild.className +
              '"></span>' +
              "<span>" +
              arrList[i].firstChild.dataset.originalTitle +
              "</span>" +
              "</div>" +
              '<span class="float-right bookmark-icon feather icon-star warning"></span>' +
              "</a>" +
              "</li>";
          }
          $("ul.search-list").append($arrList);
        } else {
          // if search input blank, hide overlay
          if (appContent.hasClass("show-overlay")) {
            appContent.removeClass("show-overlay");
          }
          // If filter box is empty
          if (searchList.hasClass("show")) {
            searchList.removeClass("show");
          }
        }
      }
    }
  });

  // Add class on hover of the list
  $(document).on("mouseenter", ".search-list li", function (e) {
    $(this)
      .siblings()
      .removeClass("current_item");
    $(this).addClass("current_item");
  });
  $(document).on("click", ".search-list li", function (e) {
    e.stopPropagation();
  });

  $("html").on("click", function ($this) {
    if (!$($this.target).hasClass("bookmark-icon")) {
      if (bookmarkSearchList.hasClass("show")) {
        bookmarkSearchList.removeClass("show");
      }
      if (bookmarkInput.hasClass("show")) {
        bookmarkInput.removeClass("show");
      }
    }
  });

  // Prevent closing bookmark dropdown on input textbox click
  $(document).on("click", ".bookmark-input input", function (e) {
    bookmarkInput.addClass("show");
    bookmarkSearchList.addClass("show");
  });

  // Favorite star click
  $(document).on("click", ".bookmark-input .search-list .bookmark-icon", function (e) {
    e.stopPropagation();
    if ($(this).hasClass("warning")) {
      $(this).removeClass("warning");
      var arrList = $("ul.nav.navbar-nav.bookmark-icons li");
      for (var i = 0; i < arrList.length; i++) {
        if (
          arrList[i].firstChild.dataset.originalTitle ==
          $(this).parent()[0].innerText
        ) {
          arrList[i].remove();
        }
      }
      e.preventDefault();
    } else {
      var arrList = $("ul.nav.navbar-nav.bookmark-icons li");
      $(this).addClass("warning");
      e.preventDefault();
      var $url = $(this).parent()[0].href,
        $name = $(this).parent()[0].innerText,
        $icon = $(this).parent()[0].firstChild.firstChild.dataset.icon,
        $listItem = "",
        $listItemDropdown = "";
      $listItem =
        '<li class="nav-item d-none d-lg-block">' +
        '<a class="nav-link" href="' +
        $url +
        '" data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
        $name +
        '">' +
        '<i class="ficon ' +
        $icon +
        '"></i>' +
        "</a>" +
        "</li>";
      $("ul.nav.bookmark-icons").append($listItem);
      $('[data-toggle="tooltip"]').tooltip();
    }
  });

  // If we use up key(38) Down key (40) or Enter key(13)
  $(window).on("keydown", function (e) {
    var $current = $(".search-list li.current_item"),
      $next,
      $prev;
    if (e.keyCode === 40) {
      $next = $current.next();
      $current.removeClass("current_item");
      $current = $next.addClass("current_item");
    } else if (e.keyCode === 38) {
      $prev = $current.prev();
      $current.removeClass("current_item");
      $current = $prev.addClass("current_item");
    }

    if (e.keyCode === 13 && $(".search-list li.current_item").length > 0) {
      var selected_item = $(".search-list li.current_item a");
      window.location = selected_item.attr("href");
      $(selected_item).trigger("click");
    }
  });

  // Waves Effect
  Waves.init();
  Waves.attach(".btn", ["waves-light"]);
})(window, document, jQuery);
