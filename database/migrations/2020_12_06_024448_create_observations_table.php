<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('id_p')->nullable();
			$table->string('local')->nullable();
			$table->string('camera')->nullable();			
			$table->date('date')->nullable();
			$table->string('hour')->nullable();
			$table->string('composition')->nullable();
			$table->string('uniform')->nullable();
			$table->string('age')->nullable();
			$table->string('gender')->nullable();
			$table->string('timeline_dp')->nullable();
			$table->string('timeline_c')->nullable();
			$table->string('dp')->nullable();
			$table->string('category')->nullable();
			$table->string('brand')->nullable();
			$table->string('product')->nullable();
			$table->string('timefind_pro')->nullable();
			$table->string('decision_pro')->nullable();
			$table->string('interaction')->nullable();
			$table->string('cart')->nullable();
			$table->string('count')->nullable();
			$table->string('shelf')->nullable();
			$table->string('TypeInteraction')->nullable();
			$table->string('NumberInteractions')->nullable();
			$table->string('TimeDesicion')->nullable();
			$table->string('ScoreTimeDesicion')->nullable();
			$table->string('TimeFind')->nullable();
			$table->string('ScoreTimeFind')->nullable();
			$table->string('ImpulseScore')->nullable();
			$table->string('ImpulseScoreForCart')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations');
    }
}
