<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DateTime;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /* Time Diffrence */
    public function timeDiffrence($start_time,$current_time){
      $start_t = new DateTime($start_time);
      $current_t = new DateTime($current_time);
      $difference = $start_t ->diff($current_t );
      $return_time = $difference ->format('%H:%I:%S');
      return $return_time;
    }
}
