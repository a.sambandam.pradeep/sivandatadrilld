<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use DateTime;

class DashboardController extends Controller
{
	public function formatPercent($data){
		return round($data);
	}
    // Dashboard - Analytics
    public function dashboardAnalytics(Request $request){

		$pageConfigs = ['pageHeader' => false];
		$regions = DB::table("observations")->select("local")->distinct("local")->get();
		$brands = DB::table("observations")->select("brand")->distinct("brand")->get();
		$products = DB::table("observations")->select("product")->distinct("product")->get();
		$local = isset($_POST['local'])?$_POST['local']:'';
		$from = isset($_POST['fromdate_submit'])?$_POST['fromdate_submit']:'';
		$to = isset($_POST['todate_submit'])?$_POST['todate_submit']:'';
		$fromdate = isset($_POST['fromdate'])?$_POST['fromdate']:'';
		$todate = isset($_POST['todate'])?$_POST['todate']:'';
		$dp = isset($_POST['dp'])?$_POST['dp']:'';
		$category = isset($_POST['category'])?$_POST['category']:'';
		$brand = isset($_POST['brand'])?$_POST['brand']:'';
		$product = isset($_POST['product'])?$_POST['product']:'';
		$noresults = 0;

		/* Age Related Queries */
		$agePercentagearr = array();
		$age1Count = DB::table('observations')->whereBetween('age', [18, 24])->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$age2Count = DB::table('observations')->whereBetween('age', [25, 34])->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$age3Count = DB::table('observations')->whereBetween('age', [35, 44])->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$age4Count = DB::table('observations')->whereBetween('age', [45, 54])->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$age5Count = DB::table('observations')->whereBetween('age', [55, 64])->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$age6Count = DB::table('observations')->where([['age', '>=', '65'],['product', '=', $product]])->whereBetween('date', [$from, $to])->count();

		$totalAgeCount = $age1Count+$age2Count+$age3Count+$age4Count+$age5Count+$age6Count;
		$productavgage = $categoryavgage = $differenceage = $productMalePercent = $productFemalePercent = $categoryMalePercent = $categoryFemalePercent = $productSinglePercent = $productFamilyPercent = $productCouplePercent = $categorySinglePercent = $categoryFamilyPercent = $categoryCouplePercent = $totalsecs = 0;
		if($totalAgeCount != 0){
			$productavgage = DB::table('observations')->where('product', $product)->whereBetween('date', [$from, $to])->select('age')->avg('age');
			$categoryavgage = DB::table('observations')->where('category', $category)->whereBetween('date', [$from, $to])->select('category')->avg('age');
			$agePercentagearr[] = $this->formatPercent(($age1Count*100)/$totalAgeCount);
			$agePercentagearr[] = $this->formatPercent(($age2Count*100)/$totalAgeCount);
			$agePercentagearr[] = $this->formatPercent(($age3Count*100)/$totalAgeCount);
			$agePercentagearr[] = $this->formatPercent(($age4Count*100)/$totalAgeCount);
			$agePercentagearr[] = $this->formatPercent(($age5Count*100)/$totalAgeCount);
			$agePercentagearr[] = $this->formatPercent(($age6Count*100)/$totalAgeCount);
			$differenceage = 0;
			if($productavgage > $categoryavgage){
				$differenceage = $productavgage-$categoryavgage;
			}
			else{
				$differenceage = $categoryavgage-$productavgage;
			}
		}
		else{
			$noresults = 1;
		}
		/* Age Related Queries */

		/* Gender Related Queries */
		$genderPercentagearr = array();
		$productMaleCount = DB::table('observations')->where('gender', 'male')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$productFemaleCount = DB::table('observations')->where('gender', 'female')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$producttotalGenderCount = $productMaleCount+$productFemaleCount;
		if($producttotalGenderCount != 0){
			$genderPercentagearr[] = $productFemalePercent = $this->formatPercent(($productFemaleCount*100)/$producttotalGenderCount);
			$genderPercentagearr[] = $productMalePercent = $this->formatPercent(($productMaleCount*100)/$producttotalGenderCount);
		}else{
			$noresults = 1;
		}

		$categoryMaleCount = DB::table('observations')->where('gender', 'male')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$categoryFemaleCount = DB::table('observations')->where('gender', 'female')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$categorytotalGenderCount = $categoryMaleCount+$categoryFemaleCount;

		if($categorytotalGenderCount != 0){
			$categoryFemalePercent = $this->formatPercent(($categoryFemaleCount*100)/$categorytotalGenderCount);
			$categoryMalePercent = $this->formatPercent(($categoryMaleCount*100)/$categorytotalGenderCount);

			if($productMalePercent > $categoryMalePercent){
				$differenceage = $productavgage-$categoryavgage;
			}else{
				$differenceage = $categoryavgage-$productavgage;
			}
		}else{
			$noresults = 1;
		}

		/* Gender Related Queries */

		/* Emotional Rational Related Queries */
		$rePercentagearr = array();
		$productCartREData = DB::table('observations')->select('decision_pro')->where('cart', 'YES')->where('product', $product)->whereBetween('date', [$from, $to])->get();
		$productRCount = $productECount = 0;
		foreach ($productCartREData as $productCartRE) {
			$time = $productCartRE->decision_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
					$totalsecs = 0;
				}
			if($totalsecs>= 10){
				$productRCount++;
			}else{
				$productECount++;
			}
		}

		$producttotalRECount = $productRCount+$productECount;
		$productEPercent = $productRPercent = $categoryRPercent = $categoryEPercent = 0;
		if($producttotalRECount != 0){
			$rePercentagearr[] = $productEPercent = $this->formatPercent(($productECount*100)/$producttotalRECount);
			$rePercentagearr[] = $productRPercent = $this->formatPercent(($productRCount*100)/$producttotalRECount);
		}else{
			$noresults = 1;
		}
		$categoryCartREData = DB::table('observations')->select('decision_pro')->where('cart', 'YES')->where('category', $category)->whereBetween('date', [$from, $to])->get();
		$categoryRCount = $categoryECount = 0;
		foreach ($categoryCartREData as $categoryCartRE) {
			$time = $categoryCartRE->decision_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])){
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
					$totalsecs = 0;
				}
			if($totalsecs>= 10){
				$categoryRCount++;
			}else{
				$categoryECount++;
			}
		}

		$categorytotalRECount = $categoryRCount+$categoryECount;
		if($categorytotalRECount != 0){
			$categoryRPercent = $this->formatPercent(($categoryRCount*100)/$categorytotalRECount);
			$categoryEPercent = $this->formatPercent(($categoryECount*100)/$categorytotalRECount);
		}else{
			$noresults = 1;
		}

		/* Emotional Rational Queries */

		/* Composition Related Queries */
		$compositionPercentagearr = array();
		$productSingleCount = DB::table('observations')->where('composition', 'one person')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$productFamilyCount = DB::table('observations')->where('composition', 'familiy')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$productCoupleCount = DB::table('observations')->where('composition', 'couple')->where('product', $product)->whereBetween('date', [$from, $to])->count();

		$producttotalCompositionCount = $productSingleCount+$productFamilyCount+$productCoupleCount;

		if($producttotalCompositionCount != 0){
			$compositionPercentagearr[] = $productSinglePercent = $this->formatPercent(($productSingleCount*100)/$producttotalCompositionCount);
			$compositionPercentagearr[] = $productFamilyPercent = $this->formatPercent(($productFamilyCount*100)/$producttotalCompositionCount);
			$compositionPercentagearr[] = $productCouplePercent = $this->formatPercent(($productCoupleCount*100)/$producttotalCompositionCount);
		}
		$categorySingleCount = DB::table('observations')->where('composition', 'one person')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$categoryFamilyCount = DB::table('observations')->where('composition', 'familiy')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$categoryCoupleCount = DB::table('observations')->where('composition', 'couple')->where('category', $category)->whereBetween('date', [$from, $to])->count();

		$categorytotalCompositionCount = $categorySingleCount+$categoryFamilyCount+$categoryCoupleCount;

		if($categorytotalCompositionCount != 0){
			$categorySinglePercent = $this->formatPercent(($categorySingleCount*100)/$categorytotalCompositionCount);
			$categoryFamilyPercent = $this->formatPercent(($categoryFamilyCount*100)/$categorytotalCompositionCount);
			$categoryCouplePercent = $this->formatPercent(($categoryCoupleCount*100)/$categorytotalCompositionCount);
		}else{
			$noresults = 1;
		}

		/* Composition Related Queries */

		/* Interaction Queries */

		$totaldpinteractioncount  = DB::table('observations')->where('dp', $dp)->whereBetween('date', [$from, $to])->count();
		$totaldpCartcount  = DB::table('observations')->where('cart', 'YES')->where('dp', $dp)->whereBetween('date', [$from, $to])->count();
		$dpInteractionPercentage = 0;
		if( $totaldpinteractioncount > 0 && $totaldpCartcount > 0){
			$dpInteractionPercentage = $this->formatPercent( ($totaldpCartcount*100)/$totaldpinteractioncount);
		}

		$totalcategoryinteractioncount  = DB::table('observations')->where('dp', $dp)->whereBetween('date', [$from, $to])->count();
		$totalcategoryCartcount  = DB::table('observations')->where('cart', 'YES')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$categoryInteractionPercentage = 0;
		if( $totalcategoryinteractioncount > 0 && $totalcategoryCartcount > 0){
			$categoryInteractionPercentage = $this->formatPercent( ($totalcategoryCartcount*100)/$totalcategoryinteractioncount);
		}


		$totalbrandinteractioncount  = DB::table('observations')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$totalbrandCartcount  = DB::table('observations')->where('cart', 'YES')->where('brand', $brand)->whereBetween('date', [$from, $to])->count();
		$brandInteractionPercentage = 0;
		if( $totalbrandinteractioncount > 0 && $totalbrandCartcount > 0){
			$brandInteractionPercentage = $this->formatPercent( ($totalbrandCartcount*100)/$totalbrandinteractioncount);
		}

		$totalproductinteractioncount  = DB::table('observations')->where('brand', $brand)->whereBetween('date', [$from, $to])->count();
		$totalproductCartcount  = DB::table('observations')->where('cart', 'YES')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$productInteractionPercentage = 0;
		if( $totalproductinteractioncount > 0 && $totalproductCartcount > 0){
			$productInteractionPercentage = $this->formatPercent( ($totalproductCartcount*100)/$totalproductinteractioncount);
		}

		$totalproductinteractioncount  = DB::table('observations')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$totalproductCartcount  = DB::table('observations')->where('cart', 'YES')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$totalAbandonCartCount = DB::table('observations')->where('cart', 'NO')->where('product', $product)->whereBetween('date', [$from, $to])->count();

		$productCartPercentage = 0;
		if($totalproductinteractioncount > 0 && $totalproductCartcount > 0 ){
			$productCartPercentage = $this->formatPercent( ($totalproductCartcount*100)/$totalproductinteractioncount);
		}

		$productAbandonPercentage = 0;
		if($totalproductinteractioncount > 0 && $totalAbandonCartCount > 0 ){
			$productAbandonPercentage = $this->formatPercent( ($totalAbandonCartCount*100)/$totalproductinteractioncount);
		}

		/* Interaction Queries */

		/* Category Impulse Queries */

		/*$categoryTimeFindData  = DB::table('observations')->select('timefind_pro')->where('category', $category)->whereBetween('date', [$from, $to])->get();
		$categoryTimeDecisionData  = DB::table('observations')->select('decision_pro')->where('category', $category)->whereBetween('date', [$from, $to])->get();
		$categoryTotal  = DB::table('observations')->select('timefind_pro')->where('category', $category)->whereBetween('date', [$from, $to])->count();

		$categoryTotalTimeFindsecs = 0;
		foreach ($categoryTimeFindData as $categoryTime) {
			$time = $categoryTime->timefind_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
				$totalsecs = 0;
			}
			$categoryTotalTimeFindsecs += $totalsecs;
		}
		$avgCategoryTotalTimeFindsecs = $avgCategoryTotalTimeDecisionsecs = $categoryImpulse = $avgbrandTotalTimeFindsecs = $avgbrandTotalTimeDecisionsecs = 0;
		if($categoryTotal != 0){
			$avgCategoryTotalTimeFindsecs = $categoryTotalTimeFindsecs/$categoryTotal;
		}else{
			$noresults = 1;
		}

		$categoryTotalTimeDecisionsecs = 0;
		foreach ($categoryTimeDecisionData as $categoryTime) {
			$time = $categoryTime->decision_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
				$totalsecs = 0;
			}
			$categoryTotalTimeDecisionsecs += $totalsecs;
		}

		if($categoryTotal != 0){
			$avgCategoryTotalTimeDecisionsecs = $categoryTotalTimeDecisionsecs/$categoryTotal;
		}else{
			$noresults = 1;
		}


		$totalcategoryIntcount  = DB::table('observations')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$totalcategoryAbandoncount  = DB::table('observations')->where('cart', 'NO')->where('category', $category)->whereBetween('date', [$from, $to])->count();
		$categoryAbandonPercentage = 0;
		if( $totalcategoryIntcount > 0 && $totalcategoryAbandoncount > 0){
			$categoryAbandonPercentage = $this->formatPercent( ($totalcategoryAbandoncount*100)/$totalcategoryIntcount);
		}

		$categoryImpulse = 100-((($avgCategoryTotalTimeFindsecs+$avgCategoryTotalTimeDecisionsecs)/2)+$categoryAbandonPercentage);*/
		$categoryImpulse  = DB::table('observations')->select('ImpulseScore')->where('category', $category)->whereBetween('date', [$from, $to])->avg('ImpulseScore');
		$categoryImpulse = 100*$categoryImpulse;
		$categoryImpulse = $this->formatPercent($categoryImpulse);

		/* Category Impulse Queries */

		/* Brand Impulse Queries */

		/*$brandTimeFindData  = DB::table('observations')->select('timefind_pro')->where('brand', $brand)->whereBetween('date', [$from, $to])->get();
		$brandTimeDecisionData  = DB::table('observations')->select('decision_pro')->where('brand', $brand)->whereBetween('date', [$from, $to])->get();
		$brandTotal  = DB::table('observations')->select('timefind_pro')->where('brand', $brand)->whereBetween('date', [$from, $to])->count();

		$brandTotalTimeFindsecs = 0;
		foreach ($brandTimeFindData as $brandTime) {
			$time = $brandTime->timefind_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
				$totalsecs = 0;
			}
			$brandTotalTimeFindsecs += $totalsecs;
		}
		if($brandTotal != 0){
			$avgbrandTotalTimeFindsecs = $brandTotalTimeFindsecs/$brandTotal;
		}else{
			$noresults = 1;
		}


		$brandTotalTimeDecisionsecs = 0;
		foreach ($brandTimeDecisionData as $brandTime) {
			$time = $brandTime->decision_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
				$totalsecs = 0;
			}
			$brandTotalTimeDecisionsecs += $totalsecs;
		}
		if($brandTotal != 0){
			$avgbrandTotalTimeDecisionsecs = $brandTotalTimeDecisionsecs/$brandTotal;
		}else{
			$noresults = 1;
		}



		$totalbrandIntcount  = DB::table('observations')->where('brand', $brand)->whereBetween('date', [$from, $to])->count();
		$totalbrandAbandoncount  = DB::table('observations')->where('cart', 'NO')->where('brand', $brand)->whereBetween('date', [$from, $to])->count();
		$brandAbandonPercentage = 0;
		if( $totalbrandIntcount > 0 && $totalbrandAbandoncount > 0){
			$brandAbandonPercentage = $this->formatPercent( ($totalbrandAbandoncount*100)/$totalbrandIntcount);
		}

		$brandImpulse = 100-((($avgbrandTotalTimeFindsecs+$avgbrandTotalTimeDecisionsecs)/2)+$brandAbandonPercentage);*/
		$brandImpulse = DB::table('observations')->select('ImpulseScore')->where('brand', $brand)->whereBetween('date', [$from, $to])->avg('ImpulseScore');
		$brandImpulse = 100*$brandImpulse;
		$brandImpulse = $this->formatPercent($brandImpulse);

		/* Brand Impulse Queries */

		/* Product Impulse Queries */

		/*$productTimeFindData  = DB::table('observations')->select('timefind_pro')->where('product', $product)->whereBetween('date', [$from, $to])->get();
		$productTimeDecisionData  = DB::table('observations')->select('decision_pro')->where('product', $product)->whereBetween('date', [$from, $to])->get();
		$productTotal  = DB::table('observations')->select('timefind_pro')->where('product', $product)->whereBetween('date', [$from, $to])->count();

		$productTotalTimeFindsecs = $avgproductTotalTimeFindsecs = $avgproductTotalTimeDecisionsecs = 0;
		foreach ($productTimeFindData as $productTime) {
			$time = $productTime->timefind_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
				$totalsecs = 0;
			}
			$productTotalTimeFindsecs += $totalsecs;
		}
		if($productTotal != 0){
			$avgproductTotalTimeFindsecs = $productTotalTimeFindsecs/$productTotal;
		}else{
			$noresults = 1;
		}


		$productTotalTimeDecisionsecs = 0;
		foreach ($productTimeDecisionData as $productTime) {
			$time = $productTime->decision_pro;
			$timearr = explode(':',$time);
			$hourstosecs = $timearr[0]*3600;
			$minstosecs = $timearr[1]*60;
			if (is_numeric($hourstosecs) && is_numeric($minstosecs) && is_numeric($time[2])) {
				$totalsecs = $hourstosecs+$minstosecs+$time[2];
			}else{
				$totalsecs = 0;
			}
			$productTotalTimeDecisionsecs += $totalsecs;
		}
		if($productTotal != 0){
			$avgproductTotalTimeDecisionsecs = $productTotalTimeDecisionsecs/$productTotal;
		}else{
			$noresults = 1;
		}


		$totalproductIntcount  = DB::table('observations')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$totalproductAbandoncount  = DB::table('observations')->where('cart', 'NO')->where('product', $product)->whereBetween('date', [$from, $to])->count();
		$productAbandonPercentage = 0;
		if( $totalproductIntcount > 0 && $totalproductAbandoncount > 0){
			$productAbandonPercentage = $this->formatPercent( ($totalproductAbandoncount*100)/$totalproductIntcount);
		}

		$productImpulse = 100-((($avgproductTotalTimeFindsecs+$avgproductTotalTimeDecisionsecs)/2)+$productAbandonPercentage);*/
		$productImpulse = DB::table('observations')->select('ImpulseScore')->where('product', $product)->whereBetween('date', [$from, $to])->avg('ImpulseScore');
		$productImpulse = 100*$productImpulse;
		$productImpulse = $this->formatPercent($productImpulse);

		/* Product Impulse Queries */

		/* Average Queries */
		$totalIntcount  = DB::table('observations')->whereBetween('date', [$from, $to])->count();
		$totalDpCartcount  = DB::table('observations')->where('cart', 'YES')->whereBetween('date', [$from, $to])->count();
		$totalDpCartPercentage = 0;
		if( $totalIntcount > 0 && $totalDpCartcount > 0){
			$totalDpCartPercentage = $this->formatPercent( ($totalDpCartcount*100)/$totalIntcount);
		}

		/* Average Queries */
		
		$recommendations = DB::table('recommendations')->where('category', $category)->orderBy('icon','DESC')->get();
		$dynamicRows = DB::table('dynamic_texts')->get();
		$tootltips = array();
		if(count($dynamicRows) > 0){
			foreach($dynamicRows as $key=>$value){
				for($j=1;$j<=16;$j++){
					$tooltip = 'tooltip'.$j;
					$tootltips[] = $value->{$tooltip};
		  		}
			}
		  }

		/* Buyers personality */
		$totalBuy = 0;
		$emotional = 0;
		$rational = 0;

		$buyers = DB::table('observations')->whereBetween('date', [$from, $to])->where('cart','YES')->distinct('id_p')->get();
		foreach ($buyers as $key => $buyers_data) {
			$buy_interval =  $this->timeDiffrence($buyers_data->timefind_pro,$buyers_data->decision_pro);
			$interval_sec=explode(':',$buy_interval);
			$totalBuy++;
			// Rational Buyers
			if($interval_sec[2]>=5){
				$rational++;
			}
			// Emotional Buyers
			else{
				$emotional++;
			}
		}
		// Buyer Percentage
		$emotional_buyers = round(($emotional/$totalBuy)*100);
		$rational_buyers = round(($rational/$totalBuy)*100);
		/* Buyers personality */
		
		/* Encouraging Products */
		$purchasers = '';
		$beforeproducts = $afterproducts = $beforedata = $afterdata = array();
		$purchasers = DB::table('observations')->select('id_p','decision_pro')->whereBetween('date', [$from, $to])->where('cart','YES')->where('product',$product)->get();
		
		foreach ($purchasers as $key => $purchaser) {
			$purchaserid = $purchaser->id_p;
			$purchasetime = $purchaser->decision_pro;
			$beforeproductsdb = DB::table('observations')->select('product')->whereBetween('date', [$from, $to])->where('cart','YES')->where('id_p',$purchaserid)->where('category',$category)->where('decision_pro', '<', $purchasetime)->get();
			foreach ($beforeproductsdb as $key => $beforeproduct) {
				if(isset($beforeproducts[$beforeproduct->product])){
					$beforeproducts[$beforeproduct->product] = $beforeproducts[$beforeproduct->product]+1;
				}else{
					$beforeproducts[$beforeproduct->product] = 1;
				}
			}
			$afterproductsdb = DB::table('observations')->select('product')->whereBetween('date', [$from, $to])->where('cart','YES')->where('id_p',$purchaserid)->where('category',$category)->where('decision_pro', '>', $purchasetime)->get();
			foreach ($afterproductsdb as $key => $afterproduct) {
				if(isset($afterproducts[$afterproduct->product])){
					$afterproducts[$afterproduct->product] = $afterproducts[$afterproduct->product]+1;
				}else{
					$afterproducts[$afterproduct->product] = 1;
				}
			}
		}
		
		arsort($beforeproducts);		
		$beforeproducts = array_slice($beforeproducts, 0, 3); 
		$totalbefore = array_sum($beforeproducts);		
		foreach ($beforeproducts as $key => $beforeproduct) {
			$percentage = 100*($beforeproduct/$totalbefore);
			$beforedata[] = $key;
			$beforedata[] = $this->formatPercent($percentage);
		}
		
		arsort($afterproducts);
		$afterproducts = array_slice($afterproducts, 0, 3); 		
		$totalafter = array_sum($afterproducts);
		foreach ($afterproducts as $key => $afterproduct) {
			$percentage = 100*($afterproduct/$totalafter);
			$afterdata[] = $key;
			$afterdata[] = $this->formatPercent($percentage);
		}		
		
		/* Encouraging Products */

		return view('/pages/dashboard-analytics', [
			'pageConfigs' => $pageConfigs,
			'noresults' => $noresults,
			'regions' => $regions,
			'brands' => $brands,
			'products' => $products,
			'local' => $local,
			'from' => $from,
			'to' => $to,
			'fromdate' => $fromdate,
			'todate' => $todate,
			'dp' => $dp,
			'category' => $category,
			'brand' => $brand,
			'product' => $product,
			'data' => implode(',',$_POST),
			'agePercentages' => implode(',',$agePercentagearr),
			'productavgage' => $productavgage,
			'categoryavgage' => $categoryavgage,
			'differenceage' => $differenceage,
			'genderPercentages' => implode(',',$genderPercentagearr),
			'productMalePercent' => $productMalePercent,
			'productFemalePercent' => $productFemalePercent,
			'categoryMalePercent' => $categoryMalePercent,
			'categoryFemalePercent' => $categoryFemalePercent,
			//'rePercentages' => implode(',',$rePercentagearr),
			'productRPercent' => $productRPercent,
			'productEPercent' => $productEPercent,
			'categoryRPercent' => $categoryRPercent,
			'categoryEPercent' => $categoryEPercent,
			'compositionPercentages' => implode(',',$compositionPercentagearr),
			'productSinglePercent' => $productSinglePercent,
			'productFamilyPercent' => $productFamilyPercent,
			'productCouplePercent' => $productCouplePercent,
			'categorySinglePercent' => $categorySinglePercent,
			'categoryFamilyPercent' => $categoryFamilyPercent,
			'categoryCouplePercent' => $categoryCouplePercent,
			'dpInteractionPercentage' => $dpInteractionPercentage,
			'totalDpCartPercentage' => $totalDpCartPercentage,
			'categoryInteractionPercentage' => $categoryInteractionPercentage,
			'brandInteractionPercentage' => $brandInteractionPercentage,
			'productInteractionPercentage' => $productInteractionPercentage,
			'productCartPercentage' => $productCartPercentage,
			'productAbandonPercentage' => $productAbandonPercentage,
			'categoryImpulse' => $categoryImpulse,
			'brandImpulse' => $brandImpulse,
			'productImpulse' => $productImpulse,
			'tootltips' => $tootltips,
			'recommendations' => $recommendations,
			'emotional_buyers'=>$emotional_buyers,
			'rational_buyers'=>$rational_buyers,
			'beforedata' => $beforedata,
			'afterdata' => $afterdata
        ]);
    }
	
    // Dashboard - Ecommerce
    public function dashboardEcommerce(){
        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('/pages/dashboard-ecommerce', [
            'pageConfigs' => $pageConfigs
        ]);
    }
	public function Comparisons(){
        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('/pages/comparisons', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}
