<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Passport::routes();
		$regions = DB::table("observations")->select("local")->distinct("local")->get();
		$local = isset($_POST['local'])?$_POST['local']:'';
		$from = isset($_POST['fromdate_submit'])?$_POST['fromdate_submit']:'';
		$to = isset($_POST['todate_submit'])?$_POST['todate_submit']:'';
		$fromdate = isset($_POST['fromdate'])?$_POST['fromdate']:'';
		$todate = isset($_POST['todate'])?$_POST['todate']:'';
		$dp = isset($_POST['dp'])?$_POST['dp']:'';
		$category = isset($_POST['category'])?$_POST['category']:'';
		$brand = isset($_POST['brand'])?$_POST['brand']:'';
		$product = isset($_POST['product'])?$_POST['product']:'';
		$user = Auth::user();
		
		view()->share('user', $user);
		view()->share('regions', $regions);
		view()->share('local', $local);
		view()->share('fromdate', $fromdate);
		view()->share('todate', $todate);
		view()->share('dp', $dp);
		view()->share('category', $category);
		view()->share('brand', $brand);
		view()->share('product', $product);
		$time = date("H");
		if ($time < "12") {
			view()->share('greetings', 'Good morning');
		} else
		/* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
		if ($time >= "12" && $time < "17") {
			view()->share('greetings', 'Good afternoon');
		} else
		/* Should the time be between or equal to 1700 and 1900 hours, show good evening */
		if ($time >= "17") {
			view()->share('greetings', 'Good evening');
		} 

    }
}
